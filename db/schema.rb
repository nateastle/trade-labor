# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20170107180939) do

  create_table "affiliations", :force => true do |t|
    t.integer  "affiliate_id"
    t.integer  "downline_member_id"
    t.float    "reward_amount"
    t.boolean  "is_rewarded_for",    :default => false
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
  end

  create_table "businesses", :force => true do |t|
    t.integer  "employer_id"
    t.integer  "employee_id"
    t.string   "title"
    t.datetime "created_at",                                                          :null => false
    t.datetime "updated_at",                                                          :null => false
    t.decimal  "average_rating",       :precision => 6, :scale => 2, :default => 0.0
    t.datetime "rated_at"
    t.text     "feedback_text"
    t.datetime "feedback_submited_at"
  end

  create_table "ckeditor_assets", :force => true do |t|
    t.string   "data_file_name",                  :null => false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    :limit => 30
    t.string   "type",              :limit => 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], :name => "idx_ckeditor_assetable"
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], :name => "idx_ckeditor_assetable_type"

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0, :null => false
    t.integer  "attempts",   :default => 0, :null => false
    t.text     "handler",                   :null => false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "failed_user_imports", :force => true do |t|
    t.string   "email"
    t.text     "validation_errors"
    t.text     "data"
    t.string   "emails"
    t.string   "categories"
    t.string   "headline"
    t.string   "website"
    t.string   "phone_number"
    t.string   "city"
    t.string   "address"
    t.string   "state"
    t.string   "zip_code"
    t.string   "country"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "identities", :force => true do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "identities", ["user_id"], :name => "index_identities_on_user_id"

  create_table "invites", :force => true do |t|
    t.integer  "sender_id"
    t.integer  "reciever_id"
    t.string   "reciever_email"
    t.string   "reciever_name"
    t.datetime "sent_at"
    t.string   "token"
    t.string   "status"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.text     "comment"
  end

  create_table "payment_details", :force => true do |t|
    t.integer  "user_id"
    t.string   "ip_address"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "card_type"
    t.date     "card_expires_on"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.float    "amount_paid"
    t.text     "paypal_response"
    t.string   "express_token"
    t.string   "express_payer_id"
  end

  create_table "pg_search_documents", :force => true do |t|
    t.text     "content"
    t.integer  "searchable_id"
    t.string   "searchable_type"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "photos", :force => true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.boolean  "is_profile_photo", :default => false
  end

  create_table "prefered_referals", :force => true do |t|
    t.integer  "user_id"
    t.integer  "referal_user_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "rates", :force => true do |t|
    t.integer  "rater_id"
    t.integer  "rateable_id"
    t.string   "rateable_type"
    t.integer  "stars",         :null => false
    t.string   "dimension"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "rates", ["rateable_id", "rateable_type"], :name => "index_rates_on_rateable_id_and_rateable_type"
  add_index "rates", ["rater_id"], :name => "index_rates_on_rater_id"

  create_table "referals", :force => true do |t|
    t.integer  "user_id"
    t.integer  "referred_user_id"
    t.integer  "skill_id"
    t.integer  "position"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "reward_transactions", :force => true do |t|
    t.integer  "affiliate_id"
    t.string   "transaction_key"
    t.text     "response"
    t.float    "first_level_reward"
    t.float    "second_level_reward"
    t.float    "total_amount"
    t.datetime "completed_at"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "schedules", :force => true do |t|
    t.boolean  "fulltime",   :default => true
    t.boolean  "nights",     :default => false
    t.boolean  "weekends",   :default => false
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.integer  "user_id"
  end

  create_table "skill_users", :force => true do |t|
    t.integer  "skill_id"
    t.integer  "user_id"
    t.boolean  "offering_for_barter", :default => false
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  create_table "skills", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "temp_users", :force => true do |t|
    t.string   "email"
    t.string   "encrypted_password"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "postal_code"
    t.string   "name"
    t.string   "address_1"
    t.string   "address_2"
    t.string   "city"
    t.string   "state"
    t.string   "contact_phone"
    t.string   "cell_phone"
    t.string   "business_name"
    t.integer  "role_id"
    t.string   "country_code"
    t.string   "membership"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "tokens", :force => true do |t|
    t.string   "value"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "user_barter_skills", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "skill_id"
    t.boolean "looking_for", :default => true
  end

  create_table "user_imports", :force => true do |t|
    t.string   "csv"
    t.string   "status",     :default => "started"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                                :default => "",    :null => false
    t.string   "encrypted_password",                                   :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                                        :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                                              :null => false
    t.datetime "updated_at",                                                              :null => false
    t.string   "postal_code"
    t.string   "name"
    t.string   "last_name"
    t.string   "first_name"
    t.string   "address_1"
    t.string   "address_2"
    t.string   "city"
    t.string   "state"
    t.string   "contact_phone"
    t.string   "cell_phone"
    t.string   "business_name"
    t.integer  "role_id"
    t.decimal  "rating_average",         :precision => 6, :scale => 2, :default => 0.0
    t.string   "affiliate_id"
    t.integer  "affiliator_id"
    t.string   "country_code"
    t.string   "state_code"
    t.text     "about_company"
    t.boolean  "is_admin",                                             :default => false
    t.string   "affiliate_token"
    t.boolean  "is_request_reward",                                    :default => false
    t.string   "linkedin_url"
    t.string   "facebook_url"
    t.string   "twitter_url"
    t.integer  "profile_theme",                                        :default => 0,     :null => false
    t.string   "username"
    t.string   "tag_line"
    t.string   "lead_email"
    t.boolean  "barter",                                               :default => false
    t.boolean  "active",                                               :default => true
    t.string   "website"
    t.boolean  "admin_uploaded",                                       :default => false
    t.boolean  "claimed",                                              :default => true
    t.float    "latitude"
    t.float    "longitude"
    t.string   "claim_token"
    t.datetime "claim_token_sent_at"
    t.string   "provider"
    t.string   "uid"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["username"], :name => "index_users_on_username", :unique => true

  create_table "w9_form_details", :force => true do |t|
    t.integer  "user_id"
    t.string   "identification_type"
    t.string   "identification_number"
    t.string   "name"
    t.string   "business_name"
    t.string   "country_code"
    t.string   "state_code"
    t.string   "address1"
    t.string   "address2"
    t.string   "city"
    t.string   "pin"
    t.text     "other_info"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "zip_codes", :force => true do |t|
    t.string   "ZipCode"
    t.string   "City"
    t.string   "State"
    t.string   "County"
    t.string   "AreaCode"
    t.string   "CityType"
    t.string   "CityAliasAbbreviation"
    t.string   "CityAliasName"
    t.string   "Latitude"
    t.string   "Longitude"
    t.string   "TimeZone"
    t.integer  "Elevation"
    t.string   "CountyFIPS"
    t.string   "DayLightSaving"
    t.string   "PreferredLastLineKey"
    t.string   "ClassificationCode"
    t.string   "MultiCounty"
    t.string   "StateFIPS"
    t.string   "CityStateKey"
    t.string   "CityAliasCode"
    t.string   "PrimaryRecord"
    t.string   "CityMixedCase"
    t.string   "CityAliasMixedCase"
    t.string   "StateANSI"
    t.string   "CountyANSI"
    t.string   "FacilityCode"
    t.string   "CityDeliveryIndicator"
    t.string   "CarrierRouteRateSortation"
    t.string   "FinanceNumber"
    t.string   "UniqueZIPName"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

end
