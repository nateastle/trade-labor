class AddTagLineToUsers < ActiveRecord::Migration
  def change
  	add_column :users , :tag_line , :string
  	add_column :users , :lead_email , :string
  end
end
