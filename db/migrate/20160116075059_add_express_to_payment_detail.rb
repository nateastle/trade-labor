class AddExpressToPaymentDetail < ActiveRecord::Migration
  def change
  	add_column :payment_details , :express_token , :string
  	add_column :payment_details , :express_payer_id , :string
  end
end
