class CreateTempLocations < ActiveRecord::Migration
  def change
    create_table :temp_locations do |t|
      t.integer :user_id
      t.string :address_1
      t.string :address_2
      t.string :postal_code
      t.string :city
      t.string :state_code
      t.string :country_code
      t.boolean :uploaded, default: false
      t.timestamps
    end
  end
end