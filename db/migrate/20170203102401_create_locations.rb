class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.integer :user_id

      t.string :address_1
      t.string :address_2
      t.string :city
      t.string :state_code
      t.string :country_code
      t.string :postal_code

      t.timestamps
    end
  end
end
