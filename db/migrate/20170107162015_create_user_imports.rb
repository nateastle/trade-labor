class CreateUserImports < ActiveRecord::Migration
  def change
    create_table :user_imports do |t|
      t.string :csv
      t.string :status
      t.timestamps
    end
  end
end
