class AddWebsiteToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :website, :string
  	add_column :users, :admin_uploaded, :boolean, default: false
  	add_column :users, :latitude, :float
  	add_column :users, :longitude, :float  	
  	add_column :users, :claimed, :boolean, default: true  	
  	add_column :users, :claimed_at, :datetime
  	add_column :users, :claim_token, :string
  	add_column :users, :claim_token_sent_at, :datetime
    add_column :users, :email_verified, :boolean, default: false
  end
end
