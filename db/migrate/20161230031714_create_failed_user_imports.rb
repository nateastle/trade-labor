class CreateFailedUserImports < ActiveRecord::Migration
  def change
    create_table :failed_user_imports do |t|
    	t.string :email
    	t.text :validation_errors
        t.text :data

    	t.string :emails
    	t.string :categories
    	t.string :headline
    	t.string :website
    	t.string :phone_number
    	t.string :city
    	t.string :address
    	t.string :state
    	t.string :zip_code
    	t.string :country
    	t.float :latitude
    	t.float :longitude

      t.timestamps
    end
  end
end
