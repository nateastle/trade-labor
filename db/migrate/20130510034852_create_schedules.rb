class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.boolean :fulltime , :default => true
      t.boolean :nights , :default => false
      t.boolean :weekends , :default => false

      t.timestamps
    end
  end
end
