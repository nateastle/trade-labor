class CreateReferals < ActiveRecord::Migration
  def change
    create_table :referals do |t|
      t.integer :user_id
      t.integer :referred_user_id	
      t.integer :skill_id
      t.integer :position
      t.timestamps
    end
  end
end
