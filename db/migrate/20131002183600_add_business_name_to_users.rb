class AddBusinessNameToUsers < ActiveRecord::Migration
  def change
  	add_column :users , :business_name , :string
  	add_index :users, :business_name
  end
end
