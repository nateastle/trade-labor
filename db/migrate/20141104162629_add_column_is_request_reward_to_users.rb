class AddColumnIsRequestRewardToUsers < ActiveRecord::Migration
  def change
    add_column :users, :is_request_reward, :boolean, :default => false
  end
end
