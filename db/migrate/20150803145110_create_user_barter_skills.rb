class CreateUserBarterSkills < ActiveRecord::Migration
  def up
  	create_table :user_barter_skills , :id => false do |t|
      t.integer :user_id	
      t.integer :skill_id
      t.boolean :looking_for, :default => true
    end  	
  end

  def down
  end
end
