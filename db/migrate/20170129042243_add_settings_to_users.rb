class AddSettingsToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :settings, :text, :default => {:email => true, :address => true, :cell_phone => true, :contact_phone => true, :website => true}.to_yaml
  end
end
