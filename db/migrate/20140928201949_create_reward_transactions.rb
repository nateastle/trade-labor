class CreateRewardTransactions < ActiveRecord::Migration
  def change
    create_table :reward_transactions do |t|
      t.integer :affiliate_id
      t.string :transaction_key
      t.text :response
      
      t.float :first_level_reward
      t.float :second_level_reward
      t.float :total_amount
      t.datetime  :completed_at

      t.timestamps
    end
  end
end
