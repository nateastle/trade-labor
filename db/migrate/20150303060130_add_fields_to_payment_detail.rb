class AddFieldsToPaymentDetail < ActiveRecord::Migration
  def change
  	add_column :payment_details, :amount_paid, :float
  	add_column :payment_details, :paypal_response, :text
  end
end
