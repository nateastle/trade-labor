class CreateSkillUsers < ActiveRecord::Migration
  def change
  	drop_table :skills_users

    create_table :skill_users do |t|
    	t.integer :skill_id
    	t.integer :user_id
    	t.boolean :offering_for_barter, :default => false

      t.timestamps
    end
  end
end
