class CreateEmailVerificationLogs < ActiveRecord::Migration
  def change
    create_table :email_verification_logs do |t|
      t.integer :user_id
      t.string  :email
      t.boolean :success
      t.boolean :valid_email
      t.text    :error
      t.text    :note      
      
      t.timestamps
    end
  end
end
