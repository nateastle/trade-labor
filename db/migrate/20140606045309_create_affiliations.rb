class CreateAffiliations < ActiveRecord::Migration
  def change
    create_table :affiliations do |t|
      t.integer :affiliate_id	
      t.integer :downline_member_id
      t.float :reward_amount
      t.boolean :is_rewarded_for, :default => false
      t.timestamps
    end
  end
end
