class CreatePreferedReferals < ActiveRecord::Migration
  def change
    create_table :prefered_referals do |t|
      t.integer :user_id
      t.integer :referal_user_id

      t.timestamps
    end
  end
end
