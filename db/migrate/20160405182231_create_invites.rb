class CreateInvites < ActiveRecord::Migration
  def change
    create_table :invites do |t|
      t.integer :sender_id
      t.integer :reciever_id
      t.string :reciever_email
      t.string :reciever_name
      t.datetime :sent_at
      t.string :token
      t.string :status

      t.timestamps
    end
  end
end
