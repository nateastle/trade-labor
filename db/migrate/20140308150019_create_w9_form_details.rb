class CreateW9FormDetails < ActiveRecord::Migration
def up
  	  
  	  create_table :w9_form_details do |t|

    		t.integer :user_id
  			t.string :identification_type 
  			t.string :identification_number
  			t.string :name
  			t.string :business_name
		    
        t.string :country_code
        t.string :state_code
        t.string :address1
        t.string :address2
        t.string :city
        t.string :pin
        t.text :other_info


		    t.timestamps
      end	

  end

  def down
  end
end
