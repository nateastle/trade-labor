class CreateTempUsers < ActiveRecord::Migration
  def change
    create_table :temp_users do |t|
    	t.string :email
    	t.string :encrypted_password
    	t.string :first_name
    	t.string :last_name    	
    	t.string :postal_code
    	t.string :name
    	t.string :address_1
    	t.string :address_2
    	t.string :city
      t.string :state	
      t.string :contact_phone
      t.string :cell_phone
      t.string :business_name
      t.integer :role_id
      t.string :country_code
      t.string :state
      t.string :membership

      # Schedule
      

      #Payment
      

      t.timestamps
    end
  end
end
