namespace :user do
  desc "Verify all user emails with api"
  task verify_emails: :environment do
    StartHubucoScript.new.run
  end
end