module Hubuco
  class SingleAPI
    BASE_URI = ENV['hubuco_api_url']
    API_KEY  = ENV['hubuco_api_key']

    attr_reader :email

    def initialize(email)
      @email = email
    end

    def call
      response =  RestClient.get(BASE_URI, { params: { email: email, api: API_KEY }})
      JSON.parse response.body
    end

  end
end