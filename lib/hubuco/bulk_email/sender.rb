module Hubuco::BulkEmail
  class Sender
    BASE_URI = 'https://www.hubuco.com/bulkapi/upload/'
    API_KEY = 'ps2SGePZqvoiSuLfaL11zKSvM'

    attr_accessor :file

    def initialize(path)
      path = '/home/sandeep/sandeep/projects/nathan/bitbucket/riszil/lib/data/hubuco/example_file.csv'
      @file = File.new(path)
    end

    def send
      response = RestClient.post(BASE_URI, { api_key: API_KEY, file_name: file_name, file_contents: file } )
      file_id = response.code == 200 ? response.body : nil
      return file_id
    end

    def file_name
      File.basename file
    end

  end
end