module Hubuco::BulkEmail
  class Reciever

    BASE_URI_PROGRESS    = 'https://www.hubuco.com/bulkapi/progress/'
    API_KEY              = 'ps2SGePZqvoiSuLfaL11zKSvM'

    attr_accessor :file_id

    def initialize(file_id = '21281')
      @file_id = file_id
    end

    def response(force = false)
      if force
        @response = RestClient.get(BASE_URI_PROGRESS, params: { api_key: API_KEY, file_id: file_id } )
      else
        @response ||= RestClient.get(BASE_URI_PROGRESS, params: { api_key: API_KEY, file_id: file_id } )
      end

      return @response
    end

    def progress
      if response.body == 'error'
        return {status: 'error'}
      else
        return fields_array.zip(response.body.split(',')) #"21281,example_file.csv,1496488013,completed,5,5"
      end
    end

    def fields_array
      [:file_id,:filename,:date_uploaded,:status,:uploaded_emails,:verified_emails]
    end

    def error?
      return progress[:status] == 'error'
    end

    def completed?
      return false if progress[:status].blank?
      return progress[:status] == 'completed'
    end

    def download_csv(type = :all)
      if completed?
        # 
        RestClient.get("https://www.hubuco.com/download/results/#{file_id}/3/#{API_KEY}")
      end
    end

  end
end