require 'clockwork'
require 'rubygems'
require File.expand_path('../../config/boot', __FILE__)
require File.expand_path('../../config/environment', __FILE__)

include Clockwork

handler {|job| puts "Sending email , Inside handler..." }

#handler {|job| QC.enqueue(job)}   # If we want to put jobs in Queue

#every(1.week, 'invites.send_reminder', :at => 'Sunday 18:00') { Invite.send_reminders }
every(1.day,"invites.destroy_all_failed_user_imports",:at => '19:50'){ FailedUserImport.destroy_all }

every(3.day,"sitemap.refresh",:at => '19:50'){ `rake RAILS_ENV=production sitemap:refresh` }