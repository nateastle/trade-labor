class CustomFailure < Devise::FailureApp
  def redirect_url
    if !params[:admin_login].blank? && params[:admin_login] == "1"
        admin_login_path
    else
        new_user_session_path
    end    
  end
  def respond
    if http_auth?
      http_auth
    else
      redirect
    end
  end
end