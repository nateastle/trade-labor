Riszil::Application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'

  get '/robots.txt' => 'welcome#robots'
  get 'express_payment' => 'users#express_payment', :as => 'express_payment'
  post 'save_locations' => 'users#save_locations', :as => 'save_locations'

  resources :temp_locations do     
  end

  resources :locations do
    collection do
      post 'temp_import'
      get 'sample_csv'
      post 'process_payment'
      get 'express_checkout'     
      get 'express_payment' 
    end     
  end

  resource :referral_card, only: [:show] do
    post 'print'
  end

  resources :referals do
    collection do
      get 'search' , :action => 'search'
    end 

    member do
      post 'move' , :action => 'move'
      delete 'remove' , :action => 'remove'
    end
  end  

  resource :search , :only => [:none] do
      #post "users" ,:action => 'search'
      get "users" ,:action => 'search'
      get "suggestions" ,:action => 'suggestions'
  end  

  resources :skills do
    collection do
      get 'autocomplete' , :action => 'autocomplete'
    end  
  end  

  resources :barters , :only => [:index] do
    collection do
      get 'search'
    end  
  end  

  match 'auth/:provider/callback', to: 'sessions#create', via: [:get, :post]
  match 'auth/failure', to: redirect('/'), via: [:get, :post]  

  get '/profile/tips' => 'welcome#profile_tips', :as => 'profile_tips'
  #get '/contests_promotions' => 'welcome#contests_promotions', :as => 'contests_promotions'
  # get '/about' => 'welcome#about_us', :as => 'about_us'
  # get '/comming_soon' => 'welcome#comming_soon'
  get '/country/subregion_options' => 'welcome#subregion_options'

  get "schedule/new"

  get "photo/new"

  get "photo/create"

  get "photo/show"

  get "affiliate_programme" => redirect("/") # "welcome#affiliate_programme", :as => 'affiliate_programme'
  
  get "what_is_riszil" =>  "welcome#what_is_riszil", :as => 'what_is_riszil'

  get "passwords/edit" => "passwords#edit", :as => 'edit_password'
  put "passwords/update" => "passwords#update", :as => 'update_password'

  devise_for :users, :controllers => {:registrations => "newregistrations",:sessions => "sessions", :omniauth_callbacks => "omniauth_callbacks"}
  
  resources :users,only:[:none] do
    member do
      post 'edit_referal'
      post 'update_referal'
    end
    resources :photos, :except => [:update, :edit] do
      member do
        post 'set_as_profile_photo' ,:action => 'set_as_profile_photo'
      end  
    end  
    resource :schedule
    member do
      post 'rate' , :action => 'rate'
      get 'report'
      #get "request_pay"
    end  
    collection do
      put 'post_feedback/:business_id' , :action => 'post_feedback' , :as => 'post_feedback'
    end
  end


  get 'membership' => "memberships#index", :as => 'membership'
  get 'membership/plans' => "memberships#new", :as => 'membership_plans'
  get 'membership/change' => "memberships#change", :as => 'change_membership'
  post 'membership/upgrade' => "memberships#upgrade", :as => 'upgrade_membership'
  get 'membership/express_checkout' => "memberships#express_checkout", :as => 'express_checkout_membership'
  get 'membership/process_payment' => "memberships#process_payment", :as => 'process_payment_membership'

  resources :invites, :only => [:new, :create] do
    collection do
      get 'preview'
    end
  end

  get 'invites/decline/:token' => "invites#decline", :as => 'decline_invite'

  resources :zipcodes do
    collection do
      get 'zip'
    end
  end
  
  #resources :affiliates 
  get "w9_form" => "affiliates#w9_form_detail"
  get "affiliate" => "affiliates#affiliate" 
  post "w9_form" => "affiliates#create_w9_form_detail"
  put "w9_form" => "affiliates#update_w9_form_detail" 
  get "contact_us" => "welcome#contact_us"
  post "send_visitor_info" => "welcome#send_visitor_info"

  root :to => "welcome#index"

  get "new_payment_detail" => "welcome#new_payment_detail"

  namespace :admin do
    get '/' => "users#dashboard" , :as => "root"
    get 'users' => "users#index" , :as => "users"
    get 'login' => "admins#login" , :as => "login" 
    get 'reward_transaction' => "admins#reward_transaction" , :as => "reward_transaction" 
              
    resources :email_verification_logs, only: [:index]           
    resources :affiliates
    resources :users do
      collection do
          get 'start_hubuco'
          get 'search',:action => "search" , :as => "search"  
          get 'import'
          get 'claimed'
          get 'recent'
          post 'upload_csv'            
      end        
      member do
          post 'pay_affiliation_reward',:action => "pay_affiliation_reward"
          put 'activate'
          put 'deactivate'
      end  
    end  
    resources :skills do
      collection do
        get 'search'
        post 'search'
        post 'merge'
        get 'new_merge'
      end
      member do
        delete 'force_destroy'
      end 
    end 
    resources :user_imports do
      collection do
        get 'failed'
      end  
      member do
        post 'retry'
      end        
    end     
  end  

  match 'fbhome',to: 'welcome#fbhome', via: [:get]

  # authenticated :user do
  #   get ":username" => "users#show", :as => 'user'
  # end

  # get ":username" => "welcome#index" , :as => 'user'

  get ":username" => "users#show", :as => 'user'

end
