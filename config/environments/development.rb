Riszil::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Log error messages when you accidentally call methods on nil.
  config.whiny_nils = true

  # Show full error reports and disable caching
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger
  config.active_support.deprecation = :log
  config.log_level = :debug

  # Only use best-standards-support built into browsers
  config.action_dispatch.best_standards_support = :builtin

  # Raise exception on mass assignment protection for Active Record models
  config.active_record.mass_assignment_sanitizer = :strict

  # Log the query plan for queries taking more than this (works
  # with SQLite, MySQL, and PostgreSQL)
  config.active_record.auto_explain_threshold_in_seconds = 0.5

  # Do not compress assets
  config.assets.compress = false

  # Expands the lines which load the assets
  config.assets.debug = true

  # Disable SSL in development
  config.after_initialize do
    SslRequirement.disable_ssl_check = true
  end

  config.after_initialize do
    ActiveMerchant::Billing::Base.mode = :test
    paypal_options = {
      :login => 'sandeep.srm23_api1.gmail.com',
      :password => '1405403069',
      :signature => 'A.42Zs-HuheiIv325WDL2-DVf3EtA7UjFgzoFdTHxoIBUd3.CikxnUqw'
    }
    ::GATEWAY = ActiveMerchant::Billing::PaypalGateway.new(paypal_options)
    ::EXPRESS_GATEWAY = ActiveMerchant::Billing::PaypalExpressGateway.new(paypal_options)
  end

  #config.action_mailer.default_url_options = { :host => 'riszil.ngrok.com' }
  config.action_mailer.default_url_options = { :host => 'localhost:3000' }
  config.action_mailer.delivery_method       = :smtp
  config.action_mailer.raise_delivery_errors = false

  # config.action_mailer.smtp_settings         = {
  #   :address        => 'smtp.mandrillapp.com',
  #   :port           => 587,
  #   :authentication => 'login',
  #   :domain         => "localhost",
  #   :user_name      => 'ersandeep0610@gmail.com',
  #   :password       => 'PTv81Vv5NLgLTTdcxQy6rg'
  # }

  config.action_mailer.smtp_settings = {
      :address => 'email-smtp.us-west-2.amazonaws.com',
      :authentication => :login,
      :user_name => 'AKIAI2KJ2PDEUULO27BA',
      :password => 'Al/3bwaNfA7hYD0p+TqhsC060ercVhli04ZcsXM7dH0N',
      :enable_starttls_auto => true,
      :domain         => "riszil.com",
      :port => 587 #465 #25

  }
  
end
