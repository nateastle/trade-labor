# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "https://www.riszil.com"
SitemapGenerator::Sitemap.create_index = true
SitemapGenerator::Sitemap.sitemaps_path = 'sitemaps/'

SitemapGenerator::Sitemap.create do
  add root_path, priority: 0.0
  # Add all users:
  User.find_each do |user|
    add user_path(user.username), :lastmod => user.updated_at
  end
end
