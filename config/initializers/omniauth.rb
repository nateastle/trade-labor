Rails.application.config.middleware.use OmniAuth::Builder do
  provider :facebook, ENV['facebook_key'], ENV['facebook_secret'], scope: 'email', info_fields: 'email, name, first_name, last_name', callback_url: ENV['facebook_callback_url']
end