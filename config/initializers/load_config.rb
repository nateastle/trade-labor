config = YAML.load_file("#{Rails.root}/config/paypal_adaptive.yml")[Rails.env]

config.each do |key, value|
  ENV[key] = value.to_s unless value.kind_of? Hash
end