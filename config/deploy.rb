require 'bundler/capistrano'
# $:.unshift(File.expand_path('./lib', ENV['rvm_path']))
require 'rvm/capistrano'

set :application, "Riszil"

set :repository, "git@bitbucket.org:nateastle/trade-labor.git"

set :rvm_ruby_string, 'ruby-2.0.0-p648' 
#set :rvm_ruby_string, 'ruby-1.9.3-p448'

set :rvm_type, :user
#set :rvm_type, :system

set :branch, "master"
set :deploy_to, "/var/www/riszil"
set :rails_env, "production"
set :app_env,   "production"

set :scm, :git
set :deploy_via, :remote_cache

set :git_enable_submodules, false

set :keep_releases, 5

set :user, "ubuntu"
#set :user, "ec2-user"

set :use_sudo, false

default_run_options[:pty] = true
ssh_options[:forward_agent] = true
ssh_options[:auth_methods] = ["publickey"]
ssh_options[:keys] = ["/home/sandeep/sandeep/projects/nathan/tradelaborkey.pem"]

#load 'deploy/assets'


server "www.riszil.com", :app, :web, :db, :primary => true


namespace :deploy do
  
  desc "Symlink shared configs and folders on each release."
  task :symlink_shared do
    #run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    run "ln -nfs /data/logs/riszil/log #{release_path}/log"
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    run "ln -s #{shared_path}/assets #{release_path}/public/assets"
    run "ln -nfs #{shared_path}/uploads #{release_path}/public/uploads"
    run "ln -nfs #{shared_path}/tmp/restart.txt #{release_path}/tmp/restart.txt"
    run "ln -nfs #{shared_path}/config/application.yml #{release_path}/config/application.yml"
    run "cd #{current_path} && bundle exec rake assets:precompile RAILS_ENV=#{rails_env}"
    # Solr  

    run "ln -nfs #{shared_path}/solr #{current_path}/solr"
    run "ln -nfs #{shared_path}/sitemaps #{current_path}/public/sitemaps"

    #run "ls -al #{current_path}/solr/pids/"
  end

  desc "Symlink database config."
  task :symlink_database_config do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  end  

  # desc "Symlink logs."
  # task :symlink_logs do
  #   run "ln -nfs /data/logs/riszil/log #{release_path}/log"
  # end  

  desc "Symlink assets."
  task :symlink_assets do
    run "ln -nfs #{shared_path}/assets #{release_path}/public/assets"
  end  

  desc "Create solr dir in shared path."
  task :setup_solr_data_dir do
    run "mkdir -p #{shared_path}/solr/data"
  end

end

namespace :solr do
  [:stop, :start, :restart ].each do |action|
    desc "#{action.to_s.capitalize} Solr"
    task action, :roles => :web do
      invoke_command "sudo /etc/init.d/solr #{action.to_s}", :via => run_method
    end
  end

  desc "reindex the whole database"
  task :reindex, :roles => :app do
    run "cd #{current_path} && #{rake} RAILS_ENV=#{rails_env} sunspot:solr:reindex" 
  end
 
end  

namespace :apache do
  [:stop, :start, :restart, :reload].each do |action|
    desc "#{action.to_s.capitalize} Apache"
    task action, :roles => :web do
      invoke_command "sudo /etc/init.d/httpd #{action.to_s}", :via => run_method
    end
  end
end


namespace :passenger do  
  desc "Restart Application"  
  task :restart do  
    run "touch #{current_path}/tmp/restart.txt"  
  end  
end  
  
namespace :sitemap do  
  desc "Refersh Sitemap"  
  task :refresh do  
    run "cd #{current_path} && #{rake} RAILS_ENV=#{rails_env} sitemap:refresh" 
    # run "bundle exec rake sitemap:refresh"  
  end  
end


after "deploy:update", "deploy:cleanup" 
#after "deploy:update", "sitemap:refresh"
after 'deploy:setup', 'deploy:setup_solr_data_dir'
after 'deploy:create_symlink', 'deploy:symlink_shared'
# after 'deploy:symlink_shared', 'deploy:symlink_database_config'
# after 'deploy:symlink_database_config', 'deploy:symlink_assets'
# after 'deploy:symlink_assets', 'assets:precompile'


