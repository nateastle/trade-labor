class UserMailer < ActionMailer::Base
  default from: "sales@riszil.com" , :date => Time.now
  helper :mail
  
  AFFILIATE_PROGRAM_SUBJECT = "Riszil - Affiliate Program"
  SEND_VISITOR_INFO_TO = "info@riszil.com"
  PAYMENT_REQUEST_TO = "payments@riszil.com"

  def send_promo_link_to_affiliate(user)
	  	@user = user

	    mail(:to => @user.email  ,
	      :subject => AFFILIATE_PROGRAM_SUBJECT )
  end 

  def welcome_email(user)
      @user = user

      mail(:to => @user.email  ,
        :subject => "Riszil - Warm welcome" )    
  end

  def send_visitor_info(contact_us)
	  	@visitor_email = contact_us[:email]
	  	@visitor_message= contact_us[:message]
	  	visitor_subject= contact_us[:subject]

	    mail(:to => SEND_VISITOR_INFO_TO  ,
	      :subject => visitor_subject )
  end
  
  def send_payment_request(user)
    @user = user
    mail(to: PAYMENT_REQUEST_TO, from: user.email, subject: 'Payment Request')
  end

  def send_invite(invite)
    @invite = invite
    @sender = invite.sender
    @reciever = invite.reciever
    mail(to: @invite.reciever_email, subject: "Invite from #{@sender.full_name}")
  end  

  def invite_reminder(invite)
    @invite = invite
    @sender = invite.sender
    @reciever = invite.reciever    
    mail(to: @invite.reciever_email, subject: "Invite reminder from #{@sender.full_name}")
  end  

  def send_referral_card(user, cards_count, cards_class)
    @user = user
    @cards_count = cards_count
    @cards_class = cards_class       
    @writing_pdf = true
    attachments["Referral card #{@user.username}.pdf"] = WickedPdf.new.pdf_from_string(
      render_to_string(pdf: 'referral_card', template: 'referral_cards/card', layout: 'pdf.html'), { }
    )

    mail(to: @user.email, subject: "Referral Card")
  end  

end
