class EmailVerificationService

  attr_accessor :user, :email

  def initialize(user)
    @user = user
    @email = user.email
  end

  def call
    verification_log_attr = { user_id: user.id, email: email }

    if success?
      if valid_email?
        user.email_verified!
        verification_log_attr.update(success: true, valid_email: true, note: 'Email has been verified')
      else
        user.destroy
        verification_log_attr.update(success: true, valid_email: false, note: 'Destroying user because email is not valid.')
      end
    else
      verification_log_attr.update(error: error_message, note: 'Hubuco call not succeeded')
    end  
    EmailVerificationLog.create(verification_log_attr)
  end

  def success?
    result.present?
  end

  def valid_email?
    result == 'ok'
  end

  def result
    api_response['result']
  end

  def error_message
    api_response['error']
  end

  private

  def api_response
    begin
      @api_response ||= hubuco_api.call
    rescue Exception => e
      @api_response = { 'error' => e.message }
    end  
  end

  def hubuco_api 
    Hubuco::SingleAPI.new(email)
  end

end


