class StartHubucoScript
  def run
    EmailVerificationLog.destroy_all
    
    User.not_email_verified.limit(19500).each do |user|
      Delayed::Job.enqueue(VerifyEmailJob.new(user.id))
    end
  end
end
