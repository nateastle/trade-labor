class VerifyEmailJob < Struct.new(:user_id)
  def perform
    user = User.where(id: user_id).first
    return if user.blank?
    EmailVerificationService.new(user).call
  end

  def queue_name
    'user_verify_email'
  end  
end
