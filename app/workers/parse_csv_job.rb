class ParseCsvJob < Struct.new(:user_import_id)
  def perform
  	begin
      user_import = UserImport.find(user_import_id)
      res = User.csv_import(user_import.csv) # Passing csv file
      user_import.status = res[:msg]
    rescue Exception => e
      user_import.status = "Exception: #{e.message}"
    end
    user_import.save 
  end

	def queue_name
    'parse_csv_queue'
  end  
end