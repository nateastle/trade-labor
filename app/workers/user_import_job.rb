class UserImportJob < Struct.new(:fields)
  def perform
  	begin
    	user = User.import(fields)
    	FailedUserImport.create_or_update(user, fields) if user.failed_import?
    rescue Exception => e
      record = FailedUserImport.where(email: email).first || FailedUserImport.new(email: email)
      record.emails = fields['emails']
      record.validation_errors = "Exception in UserImportJob: #{e.message}"
      record.save
    end 
  end

  def email
  	(fields['emails'].split.first.try(:squish) || '').downcase
  end

	def queue_name
    'user_import_queue'
  end  
end