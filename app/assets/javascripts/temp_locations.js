$(function() {
  var l = Ladda.create($(this).find("#location-pay-btn").get(0));
  
  $("#location-pay-form")
    .bind("ajax:beforeSend", function (e, data, status, xhr){
      l.start();    
    }).bind("ajax:complete", function (e, data, status, xhr){
      l.stop();    
    })
})