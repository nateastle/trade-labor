
$(function() {

  // $("#change-plan-btn a.btn-plan-select").on('click' , function(event){
  //   console.log($(this).data("downgrade"))
  //   if ($(this).data("downgrade"))
  //   {
  //     event.preventDefault()
  //     if (confirm('WARNING:: you are downgrading your plan. there is no refund for plan change. Are you sure?'))
  //       { 
  //         $.get($(this).attr('data-url'))
  //       }
  //   }
  // })


  // Ladda spinner
  apply_ladda_loader();

  // .bind("ajax:error", function (e, data, status, xhr){
  //   console.log(data)
  //   error_messages = "<div class ='alert alert-danger' style='color:red;'>Unknown error</div>";
  //   $(this).parent().find(".alert").remove();
  //   $(this).parent().append(error_messages);
  // })


  $("#user_barter").on('change',function(){
    if ($(this).is(':checked'))
    {
      $("#barter-skills").slideDown();
    }
    else
    {
      $("#barter-skills").slideUp();
    }
  });

  if ($("#user_barter").length)
   {
     if ($("#user_barter").is(':checked'))
      {
        $("#barter-skills").slideDown(); 
      }
   }

  $("#schedule-options :radio").on('change',function(){

    if ($(this).attr("id") == "user_schedule_attributes_fulltime_false" )
      {
        $("#part-time-shedules").slideDown()
        //$("#part-time-shedules :input").val(false);
      }
    else
      {
       $("#part-time-shedules").slideUp(); 
       //$("#part-time-shedules :input").val(false);
      }  
  });

  $(".phone").mask("999-999-9999");

  // For closing the flash messages
  setTimeout(function() {
    if ($("#flash-message").is(":visible"))
      {$("#flash-message").slideUp('slow')}
  }, 3000);

  $(".close").on("click", function(){
    $("#flash-message").slideUp('slow')
    //$('.flash_messages').slideUp('slow');
  });

  $(document).click(function(){
    if ($("#flash-message").is(":visible"))
      {$("#flash-message").slideUp('slow')}
  });
  // For closing the flash messages
  
  $('*[data-role=activerecord_sortable]').activerecord_sortable();

  activate_client_side_validation();
  activate_jquery_validate_engine();

  $('#copyTxt').zclip(
   {
       copy: function(){ return $('#fe_text').html(); },
       afterCopy: function()
       {
           alert( $('#fe_text').html() + " was copied to clipboard");
       }
   });

  $('#autocomplete-ajax').autocomplete({
      serviceUrl: '/search/suggestions',
      //lookup: countriesArray,
      maxHeight:220,
      minChars:2,
      lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
          var re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi');
          return re.test(suggestion.value);
      },
      onSelect: function(suggestion) {
        if ($("#welcome_search").length )
          { search_user($("#welcome_search input:text").val()); }
        else
          {
            search_barters($("#barter_search input:text").val());
          }
      },
      onHint: function (hint) {
          $('#autocomplete-ajax-x').val(hint);
      },
      onInvalidateSelection: function() {
          //$('#selction-ajax').html('You selected: none');
      }
  });

  enable_autocomplete_for_postcode($('.user_postal_code'))

  // $('#user_postal_code').autocomplete({
  //     serviceUrl: '/zipcodes',
  //     //lookup: countriesArray,
  //     // maxHeight:50,
  //     minChars:2,
  //     lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
  //         var re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi');
  //         return re.test(suggestion.value);
  //     },
  //     onSelect: function(suggestion) {

  //           var zipcode;
  //           zipcode = suggestion.value;
  //           if (zipcode.length === 5) {

  //             $.ajax({
  //               url: "/zipcodes/zip",
  //               dataType: "script",
  //               type: "GET",
  //               data: "zip=" + zipcode,
  //               success: function(result, success) {
  //                 // if (result) {
  //                 //   $("#user_city").val(result.City);
  //                 //   return $("#user_state_code").val(result.State);
  //                 // }
  //               }
  //             });
  //           }

  //     },
  //     onHint: function (hint) {
  //         //$('#autocomplete-ajax-x').val(hint);
  //     },
  //     onInvalidateSelection: function() {
  //         //$('#selction-ajax').html('You selected: none');
  //     }
  // });


  if ($("#welcome_search input:text").length > 0 ) {
    search_user($("#welcome_search input:text").val());
  }

  $("#welcome_search input:text").keyup(function() {
      search_user($(this).val());
  });


  if ($("#barter_search input:text").length > 0 ) {
    search_barters($("#barter_search input:text").val());
  }

  $("#barter_search input:text").keyup(function() {
      search_barters($(this).val());
  });


  $('.digg_pagination.users_list a').attr('data-remote', 'true')

  // For Bootstrap related stuff modal start
  
  $('[data-toggle="modal"]').click(function(e) {
	  e.preventDefault();
	  var url = $(this).attr('href');
	  $.get(url, function(data) {},"script");
  });

  // For Bootstrap related stuff modal end


  // More Less Link stuff start

    $(".more_p a").on("click",function(){
        var select = $(this);
        var parent = select.parent();
        parent.hide();
        var id = select.attr('id').split("_more_link")[0];
        $("#"+id+"_less_text").hide();
        $("#"+id+"_more_text").show();
        parent.next("p.less_p").show();
    });

    $(".less_p a").on("click",function(){
        var select = $(this);
        var parent = select.parent();
        parent.hide();
        var id = select.attr('id').split("_less_link")[0];
        $("#"+id+"_more_text").hide();
        $("#"+id+"_less_text").show();
        parent.prev("p.more_p").show();
        
    });
 // More Less Link stuff start

  // Payment , Memebership stuff start

  $("#business , #premium").click(function(){
    $.ajax({
              type: "GET",
              url: "/new_payment_detail"
                })
              .done(function( data ) {
                  $( "#payment_detail" ).html( data );
                  $( "#payment_detail" ).slideDown();
          });         
  });
  
  $("#basic").click(function(){
       $("#payment_detail").slideUp();
       $("#payment_detail").html("");
  });

  if ($("#basic").is(':checked'))
    { 
       $("#payment_detail").remove();
    }
  // Payment , Memebership stuff start


    // Affiliate related stuff start
    toggle_affiliate($("#user_is_affiliate"));

    $("#user_is_affiliate").click(function(){
      var affiliate_terms = $(this)
      toggle_affiliate(affiliate_terms);           
    });
    // Affiliate related stuff end


    // W9 form detail ///////////////////
    
    other_option_check_box();

    $(".w9_form_detail input:checkbox.other-option").on("click" , function(){
          other_option_check_box();
    });

    // W9 form detail //////////////////////
});

is_int = function(value) {
  if ((parseFloat(value) === parseInt(value)) && !isNaN(value)) {
    return true;
  } else {
    return false;
  }
};


  function apply_ladda_loader(){
    $("a.ajax-ladda-loader").bind("ajax:beforeSend", function (e, data, status, xhr){
      var ll = Ladda.create(this);
      ll.start();    
    }).bind("ajax:complete", function (e, data, status, xhr){
      var ll = Ladda.create(this);
      ll.stop();    
    })
  }

function enable_autocomplete_for_postcode(element){
  element.autocomplete({
      serviceUrl: '/zipcodes',
      //lookup: countriesArray,
      // maxHeight:50,
      minChars:2,
      lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
          var re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi');
          return re.test(suggestion.value);
      },
      onSelect: function(suggestion) {
            
            var zipcode;
            zipcode = suggestion.value;
            if (zipcode.length === 5) {

              $.ajax({
                url: '/zipcodes/zip?postal_code_input_id='+ $(this).attr('id') +'',
                dataType: "script",
                type: "GET",
                data: "zip=" + zipcode,
                success: function(result, success) {
                  // if (result) {
                  //   $("#user_city").val(result.City);
                  //   return $("#user_state_code").val(result.State);
                  // }
                }
              });
            }

      },
      onHint: function (hint) {
          //$('#autocomplete-ajax-x').val(hint);
      },
      onInvalidateSelection: function() {
          //$('#selction-ajax').html('You selected: none');
      }
  });
}

function enable_lead_email(){
  
  if ($(".lead_email_present").length)
    { 
      $(".lead_email_present").on('change',function(){
        if ( $(this).is(":checked") )
        { 
          $(this).parents("form").find("#user_lead_email").show();
        }
        else
        { 
          $(this).parents("form").find("#user_lead_email").hide();
        }
      }); 
    }
}


function enable_loader_for(element){
   element.bind('ajax:beforeSend', function(evt, xhr, settings){
          $("#loder").show();                
     }).bind("ajax:success",function(evt, data, status, xhr){
              $("#loder").hide();
     }).bind("ajax:error", function(evt, data, status, xhr){
              $("#loder").hide();
     }); 
 }    


function activate_client_side_validation(){
      $(".form_for_client_side_validation").enableClientSideValidations();
  }    

function activate_jquery_validate_engine(){
      $(".form_for_validate_engine").validationEngine();  
}   

// Affiliate related stuff start
function toggle_affiliate(affiliate_terms){
    if ( affiliate_terms.is(':checked') )  
      { $("#affiliate-container :input").attr("disabled", false);
        $("#affiliate-container").slideDown();
      }
    else
      { $("#affiliate-container").slideUp();
        $("#affiliate-container :input").attr("disabled", true);
      }   
} 
// Affiliate related stuff end


function readURL(input) {
   if (input.files && input.files[0]) {//Check if input has files. 
     var reader = new FileReader(); //Initialize FileReader. 
     reader.onload = function (e) { 
       var check_ext = $(input).val(); 
       if(check_ext.match(/(?:JPG|jpg|jpeg|JPEG|PNG|png|GIF|gif|TIF|tif|TIFF|tiff|BMP|bmp)$/)){
         //$(input).siblings("div").remove(); 
           $('#uploaded-image').html('<div><img class ="before_img" style="cursor:pointer;border-color: lightblue;margin-left:1px;border-style: solid;border-width: 5px;height: 90px;width: 120px;" src ='+e.target.result+'></div>'); 
       }else{ 
         alert( 'Invalid file!' ); 
         $(input).attr("value",""); 
         // $(input).siblings("div").remove(); 
         return false; 
       } 
     }; 
     reader.readAsDataURL(input.files[0]); 
   }  
 }
 

function search_user(qstr){

    if ( qstr.length > 2 )
       {  

        var data = {search: qstr}

          if ($(".search-input #zipcode").length)
            {
              var zipcode = $(".search-input #zipcode").val();
              data["zipcode"] = zipcode;
            }

            $.ajax({
              type: "GET",
              url: "/search/users" ,
              data: data ,
              dataType: 'script'
            });
       }  
}


function search_barters(qstr){

    if ( qstr.length > 2 )
       {  
            $.ajax({
              type: "GET",
              url: "/barters/search" ,
              data: {search: qstr} ,
              dataType: 'script'
            });
       }  
}

function other_option_check_box(){
  if ( $(".w9_form_detail input:checkbox.other-option").is(':checked') )
      {
        $(".w9_form_detail input:text.other-option-value").attr("disabled",false);
      }
  else
      {
        $(".w9_form_detail input:text.other-option-value").attr("disabled",true)     
      } 
}
