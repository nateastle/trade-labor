// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery.ui.autocomplete
//= require jquery_ujs
//= require jquery.remotipart
//= require facebook_sdk
//= require twitter/bootstrap
//= require referals
//= require welcome
//= require barters
//= require rails.validations
//= require rails.validations.simple_form
//= require jquery.tokeninput
//= require rails-timeago
//= require jquery.autocomplete.min
//= require zclip
//= require jquery.validationEngine
//= require ckeditor/init
//= require jquery.maskedinput.min
//= require spin.min
//= require ladda.min
//= require searches
//= require ckeditor/init
//= require jquery_nested_form
//= require temp_locations

// Searcg box by token input field

//= require sortable
//= require jquery.ui.sortable
//= require trade_labor






// function show_loader_on_submit(){
// 	$(".ajax_form").on("submit",function(){
// 		$("#loder").show();
// 		alert(1);
//     });
// }


$(document).ready(function() {
  $(".dropdown-toggle").dropdown();
});