function search_referals(qstr){

    if ( qstr.length > 2 )
       {  
            $.ajax({
              type: "GET",
              url: "/referals/search" ,
              data: {skill: qstr} ,
              dataType: 'script'
            });
       }  
}


$(function() {


  enable_loader_for($(".add-pr-list"));

  $('#referals-search-input').autocomplete({
      serviceUrl: '/search/suggestions',
      //lookup: countriesArray,
      maxHeight:220,
      minChars:2,
      lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
          var re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi');
          return re.test(suggestion.value);
      },
      onSelect: function(suggestion) {
          search_referals($("#referals-search-form input:text").val());
          //$('#selction-ajax').html('You selected: ' + suggestion.value + ', ' + suggestion.data);
      },
      onHint: function (hint) {
          $('#autocomplete-ajax-x').val(hint);
      },
      onInvalidateSelection: function() {
          //$('#selction-ajax').html('You selected: none');
      }
  });


  if ($("#referals-search-form input:text").length > 0 ) {
    search_referals($("#referals-search-form input:text").val());
  }

  $("#referals-search-form input:text").keyup(function() {
      search_referals($(this).val());
  });

  
});
