//= require jquery
//= require jquery.tokeninput
//= require jquery.ui.autocomplete
//= require jquery_ujs
//= require twitter/bootstrap
//= require spin.min
//= require ladda.min
//= require rails.validations
//= require jquery.autocomplete.min
//= require rails.validations.simple_form
//= require jquery.validationEngine
//= require ckeditor/init


// require_tree .

$(document).ready(function(){	 

  // Enable tokens for skill
  $("#skill-to-remove").tokenInput("/skills/autocomplete",
    { queryParam: 'term' ,
      propertyToSearch: "name",
      theme: "facebook" ,
      tokenLimit: 20 ,
      preventDuplicates: true,
      prePopulate: $("#skill-to-remove").data('load')
  });


  $('#skill-with-merge').autocomplete({
      serviceUrl: '/search/suggestions',
      //lookup: countriesArray,
      maxHeight:220,
      minChars:2,
      lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
          var re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi');
          return re.test(suggestion.value);
      },
      onSelect: function(suggestion) {
        // if ($("#skills-merge-form").length )
        //   { search_user($("#welcome_search input:text").val()); }
        // else
        //   {
        //     search_barters($("#barter_search input:text").val());
        //   }
      },
      onHint: function (hint) {
          $('#autocomplete-ajax-x').val(hint);
      },
      onInvalidateSelection: function() {
          $('#selction-ajax').html('You selected: none');
      }
  });


  enable_autocomplete_for_postcode($('.user_postal_code'));

  // For closing the flash messages
  setTimeout(function() {
    if ($("#flash-message").is(":visible"))
      {$("#flash-message").slideUp('slow')}
  }, 3000);

  $(".close").on("click", function(){
    $("#flash-message").slideUp('slow')
    //$('.flash_messages').slideUp('slow');
  });

  $(document).click(function(){
    if ($("#flash-message").is(":visible"))
      {$("#flash-message").slideUp('slow')}
  });
  // For closing the flash messages

  activate_client_side_validation();  
  activate_jquery_validate_engine();

  $('.digg_pagination.users_list a').attr('data-remote', 'true');

});  


function activate_client_side_validation(){
    $(".form_for_client_side_validation").enableClientSideValidations();
}    

function activate_jquery_validate_engine(){
    $(".form_for_validate_engine").validationEngine();  
}

function readURL(input) {
   if (input.files && input.files[0]) {//Check if input has files. 
     var reader = new FileReader(); //Initialize FileReader. 
     reader.onload = function (e) { 
       var check_ext = $(input).val(); 
       if(check_ext.match(/(?:JPG|jpg|jpeg|JPEG|PNG|png|GIF|gif|TIF|tif|TIFF|tiff|BMP|bmp)$/)){
         //$(input).siblings("div").remove(); 
           $('#uploaded-image').html('<div><img class ="before_img" style="cursor:pointer;border-color: lightblue;margin-left:1px;border-style: solid;border-width: 5px;height: 90px;width: 120px;" src ='+e.target.result+'></div>'); 
       }else{ 
         alert( 'Invalid file!' ); 
         $(input).attr("value",""); 
         $(input).siblings("div").remove(); 
         return false; 
       } 
     }; 
     reader.readAsDataURL(input.files[0]); 
   }  
 }
 
 function enable_autocomplete_for_postcode(element){
  element.autocomplete({
      serviceUrl: '/zipcodes',
      //lookup: countriesArray,
      // maxHeight:50,
      minChars:2,
      lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
          var re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi');
          return re.test(suggestion.value);
      },
      onSelect: function(suggestion) {
            
            var zipcode;
            zipcode = suggestion.value;
            if (zipcode.length === 5) {

              $.ajax({
                url: '/zipcodes/zip?postal_code_input_id='+ $(this).attr('id') +'',
                dataType: "script",
                type: "GET",
                data: "zip=" + zipcode,
                success: function(result, success) {
                  // if (result) {
                  //   $("#user_city").val(result.City);
                  //   return $("#user_state_code").val(result.State);
                  // }
                }
              });
            }

      },
      onHint: function (hint) {
          //$('#autocomplete-ajax-x').val(hint);
      },
      onInvalidateSelection: function() {
          //$('#selction-ajax').html('You selected: none');
      }
  });
}

function apply_ladda_loader(){
  $("a.ajax-ladda-loader").bind("ajax:beforeSend", function (e, data, status, xhr){
    var ll = Ladda.create(this);
    ll.start();    
  }).bind("ajax:complete", function (e, data, status, xhr){
    var ll = Ladda.create(this);
    ll.stop();    
  })
}

function show_loader(){
  $('.loading').show();
}
function hide_loader(){
 $('.loading').hide(); 
}