//= require jquery
//= require jquery.ui.autocomplete
//= require jquery_ujs
//= require bootstrap.min
//= require referals
//= require welcome
//= require barters
//= require rails.validations
//= require rails.validations.simple_form
//= require jquery.tokeninput
//= require rails-timeago
//= require jquery.autocomplete.min
//= require zclip
//= require jquery.validationEngine
//= require ckeditor/init
//= require jquery.maskedinput.min
//= require spin.min
//= require ladda.min
//= require jquery.cookie
//= require users

//= require sortable
//= require jquery.ui.sortable
//= require trade_labor

$(function() {

  // Enable tokens for skill
  $("#user_skill_tokens").tokenInput("/skills/autocomplete",
    { queryParam: 'term' ,
      propertyToSearch: "name",
      theme: "facebook" ,
      tokenLimit: 1 ,
      preventDuplicates: true,
      prePopulate: $("#user_skill_tokens").data('load')
  });

  enable_lead_email();
  
  $(".membership-plans").on('click' , function(){
    $(".membership-plans span.btn-basic").removeClass("membership-selected");
    $(this).find("span.btn-basic").addClass("membership-selected")
    $("strong#membership-name").html($(this).data("name"));
    $.cookie("selected-plan", $(this).data("membership"))
    activate_client_side_validation();
    enable_lead_email();
    enable_autocomplete_for_postcode($('.user_postal_code'))

    if ($(this).data("membership") == "premium" || $(this).data("membership") == "business")
      {enable_paid_user_fields()} 
    else
      {disable_paid_user_fields()}       

    if ($(this).data("membership") == "premium")
      {enable_paymant();activateTab();} 
    else
      {disable_paymant()}

  }); 

  $("#payment_detail li a").on('click', function(){
    var payment_option = $(this).data("payment")
    $.cookie("payment-option", payment_option)
    activateTab();
  });


  if ($('#user_membership_premium').is(":checked") || $('#user_membership_business').is(":checked"))
    { 
      enable_paid_user_fields();
    }
  else
    {disable_paid_user_fields()}


  // Payment is needed only for preminum users
  if ($('#user_membership_premium').is(":checked"))
    { 
      enable_paymant();
    }
  else
    {disable_paymant()}


  activateTab();

})


function enable_paid_user_fields(){
  $('.free-user-fields input, .free-user-fields select').attr("disabled", true);$(".free-user-fields").hide();
  $(".paid-user-fields input, .paid-user-fields select").attr("disabled", false);$(".paid-user-fields").show();
}

function disable_paid_user_fields(){
  $('.free-user-fields input, .free-user-fields select').attr("disabled", false);$(".free-user-fields").show();
  $(".paid-user-fields input, .paid-user-fields select").attr("disabled", true);$(".paid-user-fields").hide();
}

function enable_paymant(){
  $('.free-user-fields input, .free-user-fields select').attr("disabled", true);$(".free-user-fields").hide();
  $(".payment-fields input, .payment-fields select").attr("disabled", false);$(".payment-fields").show();
}

function disable_paymant(){
  $('.free-user-fields input, .free-user-fields select').attr("disabled", false);$(".free-user-fields").show();
  $(".payment-fields input, .payment-fields select").attr("disabled", true);$(".payment-fields").hide();
}

function activateTab(){

  if ($.cookie("payment-option") == undefined)
    {tab = 'credit_card';}
  else
    {tab = $.cookie("payment-option");}

  $('#payment_detail a[href="#' + tab + '"]').tab('show');
  $("#payment_detail input").attr("disabled", true);
  $("#payment_detail select").attr("disabled", true);
  $("#payment_detail .tab-content "+ "#" + tab +" input").attr("disabled", false);
  $("#payment_detail .tab-content "+ "#" + tab +" select").attr("disabled", false);
};