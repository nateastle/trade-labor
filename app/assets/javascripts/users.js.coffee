# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->
  $('#select-country-code-div select').change (event) ->
    select_wrapper = $('#state_code_wrapper')
    target = $("#select-country-code-div").data("target")
    $('select', select_wrapper).attr('disabled', true)

    country_code = $(this).val()

    url = "/country/subregion_options?parent_region=#{country_code}&target="+target
    select_wrapper.load(url)

  $('[data-toggle="tooltip"]').tooltip({html:true});
  
  
  
  $(".show_downline").click ->
    $(".downline2" + $(this)[0].id).toggle( "slow");
    if $(".downline2" + $(this)[0].id).hasClass('hide')
      $(".inner_line" + $(this)[0].id).hide("slow");
    
  $(".show_downline1").click ->
    $(".downline3" + $(this)[0].id).toggle( "slow");
   