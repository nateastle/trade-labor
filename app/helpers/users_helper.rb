module UsersHelper
  
  def visible_field?(field, current_user, user)
    #return true if user_signed_in? && user.is?(current_user)
    return user.visible_field?(field) 
  end

  def token_limit(user)

  	role_name = user.role.name

  	if user.premium_user? 
  	  	return 'null'
  	elsif user.business_user?
		   return 1
  	else
		   return 0
  	end	

  end  


  def rating_per_criteria(business) 
      rating_per_criteria_hash = {}
      Rate.where("rateable_id=? and rateable_type = ?" ,business.id,'Business').collect{|t|  rating_per_criteria_hash[t.dimension.to_sym]  = t.stars }
      rating_per_criteria_hash
  end

  def linkedin_url(user)
      user.linkedin_url.blank? ? "javascript:void(0)" : user.linkedin_url
  end  

  def facebook_url(user)
      user.facebook_url.blank? ? "javascript:void(0)" : user.facebook_url
  end

  def twitter_url(user)  
      user.twitter_url.blank? ? "javascript:void(0)" : user.twitter_url
  end

    
end
