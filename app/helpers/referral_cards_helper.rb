module ReferralCardsHelper
  def qr_code(user)
    RQRCode::QRCode.new(user_url(user.username)).to_img.resize(200, 200).to_data_url
  end
end
