module ApplicationHelper

  def admin_nav_class(link)
    recognized = Rails.application.routes.recognize_path(link)
    if recognized[:controller] == params[:controller]# && recognized[:action] == params[:action]
      'active'
    else
      ''
    end
  end

  def nav_class(link)
    recognized = Rails.application.routes.recognize_path(link)
    if recognized[:controller] == params[:controller] && recognized[:action] == params[:action]
      'active'
    else
      ''
    end
  end

  # Returns the full title on a per-page basis.
  def affiliate_email
      @affiliate_email || cookies[:affiliate_email]
  end  

  def default_meta_tags
    {
      :title       => 'Riszil (The Rating & Referral Site)',
      :description => "A new and easy way to find the professional you are looking for.View each service provider's background, ratings and feedback so you can better decide on which one is best suited for your needs.",
      :keywords    => "Riszil, Labor, Refferal",
      :separator   => "&verbar;".html_safe,
        :og => {
        :site_name => "Riszil",
        :title    => "Riszil (The Rating & Referral Site)" ,
        :type     => 'profile',
        :url      => "#{remote_url_with_port_and_protocol}" ,
        :description => "A new and easy way to find the professional you are looking for. 
        Instead of just random searching, search the referrals of someone you trust.",
        :image    =>  "#{remote_url_with_port_and_protocol}/assets/riszil_logo_200.jpg"
      }
    }
  end

  def full_title(page_title)
    base_title = "Riszil"
    if page_title.empty?
      base_title
    else
      "#{base_title} | #{page_title}"
    end
  end

  def show_city_and_state(user)
     [user.city,user.state_code].reject(&:blank?).join(', ')
  end

  def user_schedule(user)
     sch =  user.schedule
     
     if sch 
      if sch.fulltime
        "Full time"
      else
        ary = [ (sch.nights ? "Nights" : nil) , (sch.weekends ? "Weekends" : nil)].compact
        "Part time : Available in " + ary.join(" & ")
      end
     end 
  end 


  # Share this related stuff

  def remote_url_with_port_and_protocol
    request.protocol + request.host_with_port
  end

  def meta_title
    "#{@user.username.try(:titleize)} has expertise in #{@user.get_skills.join(' , ')}."
  end

  def meta_url
    "#{remote_url_with_port_and_protocol}/#{@user.username}"
  end

  def meta_description

    description = []

    if !@referred_skill_names.blank?
      description << "\n\n\n"
      description << "Please check out my highly skilled referrals in #{@referred_skill_names.join(' , ')}...etc" 
    end  

    description.join(" ")
  end

  def meta_image_url
    "#{remote_url_with_port_and_protocol}/#{@user.profile_photo_url(:profile)}"
  end

end
