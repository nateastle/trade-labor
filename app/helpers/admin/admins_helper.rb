module Admin::AdminsHelper

	def second_level_affiliate(reward_transaction)
		return 	reward_transaction.second_level_affiliate.blank? ? "None" : reward_transaction.second_level_affiliate.email
	end	

end
