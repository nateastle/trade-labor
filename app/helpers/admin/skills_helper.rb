module Admin::SkillsHelper

	def serial_number(page, per_page, index)
		(@per_page*(@page - 1)) + (index+1)
	end

end
