module Admin::UsersHelper

  def total_reward_for_basic_users(tier = :first_tier)
    return @user.total_reward_for_basic_users(tier)
  end 

  def total_reward_for_business_users(tier = :first_tier) 
    return @user.total_reward_for_business_users(tier)
  end
    
  def total_reward_for_premium_users(tier = :first_tier)
    return @user.total_reward_for_premium_users(tier)
  end 

  def total_reward(tier = :first_tier)
    return (total_reward_for_basic_users(tier) + total_reward_for_business_users(tier) + total_reward_for_premium_users(tier))
  end 

  def remaining_jobs_count
    @remaining_jobs_count ||= Delayed::Job.where(queue: 'user_import_queue', last_error: nil).count 
  end

end
