module MailHelper
  def host_root_url
    #Rails.env.development? ? 'http://ce8bf39e.ngrok.io/assets' : "#{root_url}assets"
    Rails.env.development? ? 'http://localhost:3000/' : root_url
  end

  def asset_root_url
  	"#{host_root_url}assets"
  end
end
