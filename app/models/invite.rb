class Invite < ActiveRecord::Base
  include AASM
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i

  attr_accessible :reciever_email, :reciever_name, :comment

  belongs_to :sender, :class_name => 'User'
  belongs_to :reciever, :class_name => 'User'

  validates :reciever_email, :reciever_name, :comment ,:presence => true
  validates :reciever_email, format: {with: VALID_EMAIL_REGEX, message: "Not a valid email"}
  validates :reciever_email, uniqueness: { scope: [:sender_id, :status] , message: "You have already invited." }, unless: proc { |invite| invite.declined? }
  validate  :valid_reciever

  before_create :set_fields
  after_create :send_email

  aasm :column => :status do
    state :initiated, :initial => true # Record created
    state :accepted # Invitation accepted and reciever registered.
    state :declined # Invitation declined.
    state :pending # Reciver acecpted other invite and registred to site, so similar invites converted into pending.
    state :viewed # Reciver has viewed invite for adding sender to his pr list.

    event :accept, :after_commit => :update_similar_to_pending do
      transitions :from => :initiated, :to => :accepted
    end
    
    event :decline do
      transitions :from => :initiated, :to => :declined
    end

    event :set_pending do
      transitions :from => :initiated, :to => :pending
    end

    event :set_viewed do
      transitions :from => :pending, :to => :viewed
    end

  end

  def update_similar_to_pending
    Invite.initiated.where(:email => reciever_email).update_all(:status => 'pending') if accepted?
  end

  # Call Invite.send_reminders in clockwork
  def self.send_reminders
    initiated.each do |invite|
      invite.send_reminder
    end
  end

  def send_reminder
    UserMailer.invite_reminder(self).deliver!
  end

  private
  
  def send_email
  	UserMailer.send_invite(self).deliver!
  end

  def valid_reciever
    user = User.where(:email => self.reciever_email).first
    errors.add(:base, "User with this email already exist.") if user.present?
  end

  def set_fields
  	self.sent_at = Time.now
  	self.token = random_token
  end

  def random_token
    chars = [*('A'..'Z'), *('a'..'z'), *(0..9)]
    str1 = (0..3).map {chars.sample}.join
    str2 = Digest::SHA1.hexdigest("--#{Time.now.to_s}--#{str1}--")[0,4]
    return (str1 + str2)
  end

end
