class Role < ActiveRecord::Base
  has_many :users

  attr_accessible :name

  #ROLES = {:basic => { :name => "basic" , :price => 0.00 } ,:business => { :name => "business" , :price => 50.00 }  ,:premium=> { :name => "premium" , :price => 100.00 }  }
  ROLES = {:basic => { :level => 1 , :title => "Homeowner - Service Seeker",:name => "basic" , :price => 0.00, :actual_price => 0.00 , :tag_line => "Free" , :description => "For searching, Rating & Referring </br>( no skill or contact info listed)" } ,:business => { :level => 2 ,:title => "Contractor, Tradesman or Professional",:name => "business" , :price => 0.00, :actual_price => 0.00, :tag_line => "Free" , :description => "For searching, Rating & Referring but Can list only one skill and will have contact info listed." }  ,:premium=> {:level => 3 ,:title => "Premium", :name => "premium" , :price => 50.00 , :actual_price => 50.00, :tag_line => "$ 50.00 yearly Subscription" , :description => "For searching, Rating & Referring with unlimited skills and will have contact info listed." }  }

  def self.all_roles
  	Role::ROLES.collect {|k,v| v[:name]}
  end

  def self.import
     Role.all_roles.each { |role_name| Role.find_or_create_by_name(:name => role_name)}	
  end    	
end
