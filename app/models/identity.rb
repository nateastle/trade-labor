class Identity < ActiveRecord::Base
  belongs_to :user
  attr_accessible :provider, :uid

  validates_presence_of :uid, :provider
  validates_uniqueness_of :uid, :scope => :provider

  def self.find_for_oauth(auth)
    find_or_create_by_uid_and_provider(auth.uid, auth.provider)
  end

end
