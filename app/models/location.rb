class Location < ActiveRecord::Base
  
  attr_accessible  :address_1, :address_2, :city, :state_code, :country_code, :postal_code
  
  belongs_to :user

  validates :city, :postal_code, :presence => true #, :address_1 ,:country_code , :state_code  

  scope :persisted, -> { where "id IS NOT NULL" }

  def self.pay_and_save(user, payment, ip_address, express_checkout = false)

    temp_locations = user.temp_locations
    locations = prepare(user, temp_locations)
    amount = locations.size
    return {success: false, message: 'No location selected to save'} if amount == 0

    payment_detail =  if express_checkout
                        pd = PaymentDetail.new
                        pd.express_token = payment[:express_token]
                        pd      
                      else
                        PaymentDetail.new(payment)
                      end  

    payment_detail.user = user
    payment_detail.ip_address = ip_address
    payment_detail.first_name = user.first_name
    payment_detail.last_name = user.last_name

    if payment_detail.process?(amount, user.billing_address)
      locations.map(&:save) # or use gem for bulk save so will run only one query
      temp_locations.destroy_all
      # Here we can save the payment detail for this transaction
      # But in current scenerio user can have only one payment detail
      # TODO: User can have many payment details to track each transaction
      return {success: true, message: 'Payment completed successfully'}
    else
      return {success: false, message: payment_detail.errors.full_messages.join(', ')}
    end
 
  end

  def self.prepare user, temp_locations
    temp_locations.map do |temp_location| 
      loc_attr = temp_locs_attrs temp_location
      location = Location.new(loc_attr) 
      location.user = user
      location
    end
  end

  def self.temp_locs_attrs temp_loc
    temp_loc.attributes.tap{|x| x.delete('id');x.delete('uploaded');x.delete('user_id');x.delete('created_at');x.delete('updated_at') } 
  end  

end
