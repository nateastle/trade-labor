class Token < ActiveRecord::Base
  # attr_accessible :title, :body

  before_create :set_value

  def generate_random_token
      chars = [*('A'..'Z'), *('a'..'z'), *(0..9)]
      str1 = (0..3).map {chars.sample}.join
      str2 = Digest::SHA1.hexdigest("--#{Time.now.to_s}--#{str1}--")[0,4]
      return (str1 + str2)
  end

  def set_value
  	  self.value = generate_random_token
  end	

end
