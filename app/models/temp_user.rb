class TempUser < ActiveRecord::Base
   attr_accessor :password, :password_confirmation
   attr_accessible :email, :first_name, :last_name, :postal_code, :address_1, :address_2, :city, :state, :contact_phone, :cell_phone, :business_name, :country_code, :state

  def initialize_user
    user = User.new(attributes.slice("email", "first_name", "last_name", "postal_code", "address1", "address2", "city", "state", "contact_phone", "cell_phone", "business_name", "country_code", "state"))
    user.temp_user_id = self.id
    user.encrypted_password = encrypted_password
    user.membership = membership
    user.role_id = role_id    
    return user
  end

end
