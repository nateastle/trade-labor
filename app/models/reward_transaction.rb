class RewardTransaction < ActiveRecord::Base
  
  attr_accessible :response

  belongs_to :affiliate , :class_name => "User"

  before_create :generate_transaction_key

  serialize :response, Hash

  def generate_transaction_key
  	  self.transaction_key = random_string_of_18_chars	
      self.completed_at = Time.now
  end	

  def random_string_of_18_chars
	  random_string = loop do
	    random_number = SecureRandom.hex(9)
	    break random_number unless RewardTransaction.exists?(transaction_key: random_number)
	  end
	  return random_string
  end

  def second_level_affiliate
      self.affiliate.try(:affiliate)
  end  

end
