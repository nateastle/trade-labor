class TempLocation < ActiveRecord::Base
  attr_accessible  :address_1, :address_2, :city, :state_code, :country_code, :postal_code, :uploaded
  belongs_to :user

  validates :city, :postal_code, :presence => true #, :address_1 ,:country_code , :state_code 

  scope :uploaded,  where(uploaded: true)
  scope :manual,  where(uploaded: false)

  def self.saved? locs
    locs.first.try(:persisted?)
  end

end
