class PaymentDetail < ActiveRecord::Base
  # attr_accessible :title, :body
  attr_accessible  :ip_address, :user_id, :first_name , :last_name , :card_type ,:card_number,:card_verification,:card_expires_on, :upgrading_to

  attr_accessor :card_number, :card_verification , :upgrading_to
  
  serialize :paypal_response, Hash

  belongs_to :user
  validate :validate_card, if: proc { |payment| payment.express_token.blank? }
  
  #before_validation :set_detail
  #has_many :transactions, :class_name => "PaymentTransaction"

  # def set_detail
  #   express_token = user.express_token if express_checkout?
  # end

  # def express_checkout?
  #   !user.try(:express_token).blank?
  # end

  def process?(amount,billing_address)
    @billing_address = billing_address

    if self.express_token.blank?
      payment_success?(amount)
    else
      express_payment_success?(amount)
    end
  end

  def express_payment_success?(amount)
      response = EXPRESS_GATEWAY.purchase(price_in_cents(amount), express_purchase_options)
      #cart.update_attribute(:purchased_at, Time.now) if response.success?
      
      if !response.success?
        errors.add :base, "Invalid tokan"
        return false
      else
        self.amount_paid = amount
        self.paypal_response = response.as_json["params"]
        return true
      end 

    rescue Exception => e
      errors.add :base, "Error in Payment #{e.message}"
      return false
  end

  def express_token=(token)
    self[:express_token] = token
    if new_record? && !token.blank?
      # you can dump details var if you need more info from buyer
      details = EXPRESS_GATEWAY.details_for(token)
      self.express_payer_id = details.payer_id
    end
  end

  def payment_success?(amount)
      if credit_card.valid?
    	    response = GATEWAY.purchase(price_in_cents(amount), credit_card, purchase_options)

    	   if !response.success?
            errors.add :base, "Invalid Card Detail"
            return false
         else
            self.amount_paid = amount
            self.paypal_response = response.as_json["params"]
            return true
         end 
    	else
    	    credit_card.errors.full_messages.each do |message|
               errors.add :base, message
            end
    	    return false	
    	end 
  	rescue Exception => e
  	   errors.add :base, "Error in Payment #{e.message}"
       return false
  end
  
  def price_in_cents(amount)
      return (amount*100).round
  end


  def validate_card
    unless credit_card.valid?
      credit_card.errors.full_messages.each do |message|
        errors.add :base, message
      end
    end
  end

  private
  
  def express_purchase_options
    {
      :ip => ip_address,
      :token => express_token,
      :payer_id => express_payer_id
    }
  end

  def purchase_options
    {
      :ip => ip_address,
      :billing_address => @billing_address
    }
  end
  
  
  def credit_card
    @credit_card ||= ActiveMerchant::Billing::CreditCard.new(
      :brand               => card_type,
      :number             => card_number,
      :verification_value => card_verification,
      :month              => card_expires_on.month,
      :year               => card_expires_on.year,
      :first_name         => first_name,
      :last_name          => last_name
    )
  end 
end
