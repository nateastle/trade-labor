class User < ActiveRecord::Base

  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable, :omniauth_providers => [:facebook]

  attr_accessible :other_locations, :address_1, :address_2, :state, :payment_detail_attributes, :express_token,
  :barter,:email, :password, :password_confirmation, :remember_me, :postal_code, :name, 
  :first_name, :last_name,:business_name, :address_1, :address_2, :city, :country_code , 
  :state_code, :contact_phone, :cell_phone, :skill_ids, :terms_of_service,
  :terms_of_affiliate , :is_affiliate, :schedule_attributes, :photos_attributes, :offering_skill_tokens,
  :barter_skill_tokens ,:skill_tokens,:membership,:ip_address, :rating_average  ,
  :about_company, :affiliate_email ,:facebook_url, :twitter_url, :linkedin_url, 
  :profile_theme,:lead_email_present,:lead_email, :tag_line, :invite_token, :website, :settings, :locations_csv,
  :locations_attributes
 

  attr_accessor :invite_token, :temp_user_id, :express_token,:ip_address,:lead_email_present,:membership , :ip_address , :is_affiliate , :affiliate_email , :forwarded_token 

  attr_reader :skill_tokens, :barter_skill_tokens,  :offering_skill_tokens

  PROFILE_THEME = { default: 0, carpenter: 1, electrical: 2, plumber: 3  , mechanic: 4 , painter: 5 , brick: 6 , landscap: 7, construction: 8, hvac: 9, medical: 10}
  MINIMUM_REWARD_AMOUNT = 5

  serialize :settings, Hash
  mount_uploader :locations_csv, AttachmentUploader

  # has_many :user_barter_skills, :dependent => :destroy
  has_and_belongs_to_many :barter_skills,
      class_name: "Skill",
      foreign_key: "user_id",
      association_foreign_key: "skill_id",
      :join_table => "user_barter_skills"

  has_many :sent_invites, :foreign_key => 'sender_id', :class_name => "Invite"
  has_many :recieved_invites, :foreign_key => 'reciever_id', :class_name => "Invite"      

  has_many :photos, :dependent => :destroy
  has_many :referals , :order => "position ASC", :dependent => :destroy
  has_many :referred_users , :through => :referals
  has_many :referred_skills , :through => :referals

  has_many :referred_by_referals, :foreign_key => 'referred_user_id', :class_name => "Referal", :order => "position ASC", :dependent => :destroy
  has_many :referred_by_users, :through => :referred_by_referals, :source => :user  

  #has_many :prefered_referals, :dependent => :destroy
  has_one :schedule, :dependent => :destroy
  
  has_many :skill_users, :dependent => :destroy
  has_many :skills , :through => :skill_users, :uniq => true

  # has_and_belongs_to_many :skills

  has_one :payment_detail, :dependent => :destroy
  #has_many :payment_transactions, :dependent => :destroy
  has_one :w9_form_detail , :dependent => :destroy 
  belongs_to :role
  has_many :employee_businesses , :foreign_key => 'employee_id' , :class_name => "Business" , :order => "rated_at DESC"
  has_many :employer_businesses , :foreign_key => 'employer_id', :class_name => "Business" , :order => "rated_at DESC"
  has_many :employer , through: :business , :source => :user
  has_many :reward_transactions, :foreign_key => 'affiliate_id'
  # Association for Affiliate Programe
  has_many :affiliations  , :foreign_key => "affiliate_id", :dependent => :destroy
  has_many :downline_members , :through => :affiliations , :source => :downline_member
  has_one :reverse_affiliation , :foreign_key => "downline_member_id",:class_name => "Affiliation"
  has_one :affiliate , :through => :reverse_affiliation
  has_many :temp_locations
  has_many :locations
  #has_one :affiliate , :through => :reverse_affiliation

  accepts_nested_attributes_for :locations, reject_if: proc { |attributes| attributes['id'].blank? or attributes['city'].blank? or attributes['postal_code'].blank? }, allow_destroy: true
  accepts_nested_attributes_for :skills
  accepts_nested_attributes_for :schedule
  accepts_nested_attributes_for :photos, :reject_if => proc { |attributes| attributes['name'].blank? }
  accepts_nested_attributes_for :payment_detail, reject_if: :all_blank

  validates :business_name, format: { with: /\A[a-zA-Z0-9\_\&\'\s\:\-\,\\\/\(\)\|\%\$\#\.\!\+\?\@\;\~]+\z/,
    message: "Not in valid format" },:allow_blank => true
  #validates_uniqueness_of :business_name, if: Proc.new { |user| user.business_name.present? }  

  validates :email,:presence => true ,  :uniqueness => true
  # validates :city ,:postal_code,:presence => true #, :address_1 ,:country_code , :state_code  
  #validates_confirmation_of :password
  #validates :postal_code, numericality: { only_integer: true }
  validates_acceptance_of :terms_of_service, :allow_nil => false, :message => "Please accept the terms and conditions", :on => :create, :unless => Proc.new { |user| user.new_record? && !user.payment_detail.blank? && user.payment_detail.valid? }
  validate :website_validator
  validate :affiliate_email_exist_if_present
  validate :valid_postal_code
  validate :membership , :presence => true
  validates_inclusion_of :membership, in:  Role::ROLES.collect {|role| role[1][:name] } , :on => :create
  validate :validate_skills_on_role_basis
  validate :social_media_url_validator
  validate :lead_email_user
  validate :validate_barter_skills
  validate :validate_name
  validates_associated :payment_detail,  if: Proc.new { |user| user.is_paid_user? }, :on => :create
  
  before_validation :format_urls
  before_validation :set_username
  before_validation :auto_increase_if_username_already_exist, :if => Proc.new {|user| !username.blank? }

  #before_save :reset_business_name
  #before_create :set_re, :if => Proc.new {|user| !affiliate_email.blank? }
  after_save :reindex_user!
  after_save :destroy_skills_for_basic_user
  #after_save :import_locations

  before_save :set_name
  before_save :parse_about_company_field
  
  before_create :set_profile_photo_if_present
  # Callbacks for Affiliate Programe
  before_create :create_affiliation , :if => :affiliated_by_someone?
  after_create :add_affiliator_in_pr_list , :if => :affiliated_by_someone?

  before_create :set_affiliate_token ,:if => :interested_to_join_affiliate_programe?
  after_create :destroy_forwarded_token_if_present
  
  after_create :set_invite_accepted, :if => Proc.new {|user| user.invite_token.present? }
  #after_create :send_email_to_affiliate
  after_create :send_welcome_email
  after_create :remove_temp_user_if_exist
  
  before_create :set_default_schedule_if_not_present
  
  RADIUS = 25 
  ajaxful_rater

  scope :pending_rewards , where("affiliations.is_rewarded_for = ? AND affiliations.reward_amount > 0",false)
  scope :pending_rewards , where("affiliations.is_rewarded_for = ?",false)
  scope :past_rewards , where("affiliations.is_rewarded_for = ? AND affiliations.reward_amount > 0",true)
  
  scope :not_admin, where('is_admin = ?', false)
  scope :basic, lambda { where(role_id: Role.where(name: Role::ROLES[:basic][:name]).first.try(:id)) }
  scope :business, lambda { where(role_id: Role.where(name: Role::ROLES[:business][:name]).first.try(:id)) }
  scope :premium, lambda { where(role_id: Role.where(name: Role::ROLES[:premium][:name]).first.try(:id)) }

  scope :not_email_verified, where(email_verified: false)
  scope :claimed, where(claimed: true)
  scope :created_in_last, ->(days) { where("created_at > ?", Time.now - days.days) }

  # scope :basic , where("role_id = ?",Role.find_by_name(Role::ROLES[:basic][:name]).id)
  # scope :business , where("role_id = ?",Role.find_by_name(Role::ROLES[:business][:name]).id)
  # scope :premium , where("role_id = ?",Role.find_by_name(Role::ROLES[:premium][:name]).id)
  
  # TODO : We can calculcate lat , long at the time of registration so we will
  #not need a saprate zipcoe model and an "additional query".


  after_initialize do
    if self.new_record?
      membership = Role::ROLES[:basic][:name] if membership.blank?
      build_schedule if schedule.blank?
      build_payment_detail if payment_detail.blank?  
      payment_detail.first_name = first_name if payment_detail.first_name.blank?
      payment_detail.last_name = last_name if payment_detail.last_name.blank?
    end
  end

  def temp_locs_attrs
    temp_locations.map do |temp_loc|
      temp_loc.attributes.tap{|x| x.delete('id');x.delete('uploaded');x.delete('user_id');x.delete('created_at');x.delete('updated_at') }
    end    
  end

  def save_temp_locations(locations_params)
    TempLocation.transaction do 
      # Destroy existing locations if any
      temp_locations.destroy_all

      locations_params.each do |loc_params|
        temp_locations.create(loc_params.tap{|x| x.delete(:_destroy)})
      end
    end  
    temp_locations
  end

  def other_locations=(value)
    @other_locations = value
  end  

  def other_locations
    @other_locations.present? || self.locations.persisted.present?
  end  

  def import_temp_locations!
    return if locations_csv.blank?
    result = {}
    begin  
      if locations_csv && locations_csv.respond_to?(:read)
        CSV.foreach(locations_csv.path, :headers => true, encoding:'iso-8859-1:utf-8') do |row|
          hh = row.to_hash
          row = Hash[hh.map { |k, v| [k.downcase.split.join('_'), v] }]
          row[:uploaded] = true
          self.temp_locations.create(row)
        end
        result[:success] = true
        result[:msg] = 'Temp locations imported successfully'
        result[:temp_locations] = self.reload.temp_locations
      else
        result[:success] = false
        result[:msg] = 'Not a valid csv'  
        result[:temp_locations] = []    
      end
    rescue Exception => e
      result[:success] = false
      result[:msg] = e.message       
      result[:temp_locations] = []
    end 
    result 
  end

  def self.from_omniauth(auth, signed_in_resource = nil)

    identity = Identity.find_for_oauth(auth)
    user = signed_in_resource ? signed_in_resource : identity.user

    if user.nil?

      email_is_verified = auth.info.email && (auth.info.verified || auth.info.verified_email)
      email = auth.info.email if email_is_verified
      user = User.where(:email => email).first if email

      # Create the user if it's a new registration
      if user.nil?
        user = User.new(email: auth.info.email)
        user.first_name = auth.info.first_name
        user.last_name = auth.info.last_name
        password = Devise.friendly_token[0,20]
        user.password = password
        user.password_confirmation = password
        user.membership = 'business'
        user.add_role Role::ROLES[:business][:name]
        user.admin_uploaded = false
        user.terms_of_service = '1'
        user.claimed = true
        user.payment_detail = nil
        user.country_code = 'USA'
        user.skip_confirmation!
        # user.latitude = row['latitude']
        # user.longitude = row['longitude']  
        # user.city = row['city']
        # user.state_code = row['state']   
        # user.postal_code = row['zip_code'] 
        user.save
        # If not save because city and postal code are not present, then save forcefully
        unless user.persisted?
          if user.errors.keys == [:city, :postal_code]
            user.save(validate: false) 
          end
        end
      end

      user.claim! unless user.claimed?
    end

    # Associate the identity with the user if needed
    if identity.user != user
      identity.user = user
      identity.save!
    end
    user
  end

  def settings=(val)
    self[:settings][:email] = val[:email].nil? ? false : true
    self[:settings][:address] = val[:address].nil? ? false : true
    self[:settings][:cell_phone] = val[:cell_phone].nil? ? false : true
    self[:settings][:contact_phone] = val[:contact_phone].nil? ? false : true
    self[:settings][:website] = val[:website].nil? ? false : true
  end
    
  def visible_field?(field)
    return settings[:email] if field == :email
    
    return settings[:address] if field == :address
    
    return settings[:cell_phone] || settings[:contact_phone] if field == :phone

    return settings[:cell_phone] if field == :cell_phone

    return settings[:contact_phone] if field == :contact_phone  
     
    return settings[:website] if field == :website
    
    return true     
  end

  ## Hubuco start
  def email_verified!
    update_attribute(:email_verified, true)
  end
  ## Hubuco end

  def validate_name
    if business_name.blank?
      errors.add(:base, 'First name and last name can not be blank') if first_name.blank? or last_name.blank?
    end
  end

  def self.csv_import file
    result = {}
    begin  
      if file && file.respond_to?(:read)
        CSV.foreach(file.path, :headers => true, encoding:'iso-8859-1:utf-8') do |row|
          hh = row.to_hash
          row = Hash[hh.map { |k, v| [k.downcase.split.join('_'), v] }]
          Delayed::Job.enqueue(UserImportJob.new(row))
          # user = import(row)
          # FailedUserImport.create_or_update(user, row) if user.failed_import?
        end
        result[:success] = true
        result[:msg] = 'Users imported successfully'
      else
        result[:success] = false
        result[:msg] = 'Not a valid csv'      
      end
    rescue Exception => e
      result[:success] = false
      result[:msg] = e.message       
    end 
    result 
  end      

  def self.import row
    return if row['emails'].blank?
    row['emails'] = (row['emails'].split.first.try(:squish) || '').downcase
    record = User.where(email: row['emails'] ).first
    return record if record.present?
    skill = Skill.insert_and_return(row['categories']).try(:first)
    user = User.init_import(row['emails'])

    user.skills = [skill] if skill.present?
    user.business_name = row['headline']
    user.website = row['website']
    user.contact_phone = row['phone_number']
    user.city = row['city']
    user.address_1 = row['address']
    user.state_code = row['state']
    user.postal_code = row['zip_code']
    user.country_code = row['country']
    user.latitude = row['latitude']
    user.longitude = row['longitude']
    user.payment_detail = nil
    user.save
    return user
  end   

  def failed_import?
    !persisted? && errors.any? 
  end

  def self.init_import(email)
    user = User.new(email: email)
    user.password = Devise.friendly_token.first(9)
    user.password_confirmation = user.password
    user.membership = 'business'
    user.add_role Role::ROLES[:business][:name]
    user.admin_uploaded = true
    user.terms_of_service = '1'
    user.claimed = false
    user.skip_confirmation!
    user
  end
  

  # def self.open_spreadsheet(file)
  #   case  case File.extname(file.original_filename)
  #   when '.csv' then Csv.new(file.path, nil, :ignore)
  #   when '.xls' then Excel.new(file.path, nil, :ignore)
  #   when '.xlsx' then Excelx.new(file.path, nil, :ignore)
  #   else raise "Unknown file type: #{file.original_filename}"
  #   end
  # end

  def claim!
    self.claimed = true
    self.claimed_at = Time.now
    self.save!
  end

  def claimed?
    claimed
  end

  def website_validator
    if website.present?
      errors[:website] << 'Not a valid website url' unless website_valid?
    end  
  end

  def website_valid?
    # !!website.match(/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-=\?]*)*\/?$/)
    true
  end

  def set_profile_photo_if_present
    if self.new_record? && self.photos.present?
      photo = self.photos.first
      photo.is_profile_photo = true
    end
  end

  def password_required?
    # When creating user by express checkout
    if new_record? && !payment_detail.blank? && payment_detail.valid?
      false
    else
      super
    end  
  end 

  def remove_temp_user_if_exist
    if temp_user_id
      temp_user = TempUser.find(temp_user_id)
      temp_user.destroy
    end
  end

  def create_temp_user
    temp_user = TempUser.new(temp_user_attributes)
    temp_user.encrypted_password = encrypted_password
    temp_user.membership = membership
    temp_user.role_id = role_id
    temp_user.save
    return temp_user
  end

  def temp_user_attributes
    attributes.slice(
      "email", "first_name", "last_name", "postal_code", 
      "address_1", "address_2", "city", "state", "contact_phone", 
      "cell_phone", "business_name", "country_code", "state"
      )
  end

  def destroy_skills_for_basic_user
    if  basic_user? && skills.size > 0
      skills.destroy_all
    end
  end

  def validate_barter_skills
    if self.barter
      if self.barter_skills.blank? && self.offering_skills.blank?
        errors.add(:base , "If barter is selected then barter skill can not be empty")
      end
    end 
  end

  def set_default_schedule_if_not_present
    if self.new_record?
      if self.schedule.blank?
        self.build_schedule
      end
    end
  end

  def reset_business_name
    self.business_name = self.business_name.downcase.split.join('_') if !self.business_name.blank?
  end

  def get_name_for_url
    [first_name,last_name].reject {|g| g.blank?}.join(" ").downcase.split.join('_')
  end

  def set_username
    if business_name.blank?
      self.username = get_name_for_url if first_name_changed? or last_name_changed?
    else
      self.username = self.business_name.gsub(/[^a-z1-9&']+/i, '_').downcase.split.join('_') if business_name_changed?
    end
  end

  def auto_increase_if_username_already_exist
    self.username = next_username(self.username) if user_with_same_username.present? and user_with_same_username.id != self.id
  end

  def user_with_same_username(current_username = self.username)
    User.where("username = ?",current_username).first
  end

  def next_username(current_username)
    username_to_check = new_username_to_check(current_username)
    return username_to_check if user_with_same_username(username_to_check).blank?
    next_username(username_to_check)  
  end

  def new_username_to_check(current_username)
    array_of_words_of_username = current_username.split("_")
    current_key_index = array_of_words_of_username.last.to_i
    next_key_index = (current_key_index + 1).to_s
    index = (current_key_index == 0) ? array_of_words_of_username.size : (array_of_words_of_username.size - 1)
    array_of_words_of_username[index] = next_key_index
    array_of_words_of_username.join("_")
  end

  def parse_about_company_field
    scheme = Rails.env.production? ? 'https://' : 'http://'
    self.about_company = about_company.gsub('http://',scheme) if about_company.present?
  end

  def default_profile_theme?  
    profile_theme == :default
  end  

  def profile_theme
    PROFILE_THEME.key(read_attribute(:profile_theme))
  end
 
  def profile_theme=(pt)
    write_attribute(:profile_theme, PROFILE_THEME[pt.to_sym])
  end

  def referred_users_for(skill_name)
    referred_users.joins(:skills).where("skills.name = ?", skill_name)
  end

  def can_refer? user
    return false if self.is? user
    return false if user.skills.blank?
    # We may have to remove one condition out of following two
    return false if already_refered_for_all_skills?(user) # One user can be refred for more then one skills
    return true
  end

  # Check if 'user' has already beeen referd for his all current skills, bu current user
  def already_refered_for_all_skills?(user)
    refered_skill_ids_for(user) == user.skills.map(&:id)
  end

  # skill id array, for which 'user' has already been referd by current user
  def refered_skill_ids_for(user)
    referals.where("referred_user_id = ?",user.id).map(&:skill_id)
  end

  def refer(user, skill = nil ,force = false)
    if can_refer? user
      if force
        referal = referals.find_or_initialize_by(:skill_id => (skill || user.skills.first).id)
        referal.add_refered_user!(user)
        return {:success => true , :message => "You have successfully added referal for #{skill.name}" , :referal => referal}
      else
        if is_referal_already_exist_with_skill?(skill)
          return {:success => false , :message => "You have already added referal for #{skill.name}"}
        else
          referal = self.referals.build #(:skill_id => skill.id)
          referal.skill = skill
          referal.add_refered_user!(user)
          return {:success => true , :message => "You have successfully added referal for #{skill.name}" , :referal => referal}
        end
      end
    else
      return {:success => false , :message => "User do not have skills to refer"}  
    end
  end

  def is_referal_already_exist_with_skill?(skill)
    referals.map(&:skill_id).include?(skill.id) 
  end

  def is?(user)
    self.id == user.id
  end  

  def lead_email_user
    if !lead_email.blank?
      lead_email_user = User.find_by_email(self.lead_email)
      if lead_email_user.blank?
        self.errors.add(:base , "Lead email user does not exist")
      elsif !["business","premium"].include? lead_email_user.role_name
        self.errors.add(:base , "Lead email user is not a paid user")
      end  
    end  
  end

  def affiliate_email_exist_if_present
      if !self.affiliate_email.blank?
        if User.find_by_email(self.affiliate_email).blank?
            self.errors.add(:base , "Referer does not exist")
        end  
      end  
  end  

  def format_urls
      self.linkedin_url = "https://#{self.linkedin_url}" if !self.linkedin_url.blank? && self.linkedin_url[/^https?/].blank?
      self.facebook_url = "https://#{self.facebook_url}" if !self.facebook_url.blank? && self.facebook_url[/^https?/].blank?  
      self.twitter_url = "https://#{self.twitter_url}" if !self.twitter_url.blank? && self.twitter_url[/^https?/].blank?
      self.website =  "https://#{self.twitter_url}" if !self.website.blank? && self.website[/^https?/].blank?
  end  

  def social_media_url_validator

      if !self.linkedin_url.blank? && !url_valid?(self.linkedin_url)
        errors[:linkedin_url] << "Invalid url" 
      end

      if !self.facebook_url.blank? && !url_valid?(self.facebook_url)  
        errors[:facebook_url] << "Invalid url" 
      end

      if !self.twitter_url.blank? && !url_valid?(self.twitter_url)
        errors[:twitter_url] << "Invalid url" 
      end  
  end
  
  def url_valid?(url)
      !!url.match(/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-=\?]*)*\/?$/)
  end  

  def profile_photo_url(version)
      if self.profile_photo.blank?  
        "/assets/#{version.to_s}_avtar.jpg"
      else  
        self.profile_photo.name_url(version)
      end  
  end  

  def profile_photo
    self.photos.profile_photo.first
  end

  def set_name
      self.name = "#{first_name} #{last_name}".strip
  end  

  def business_or_full_name
      business_name.blank? ? name : business_name
  end  

  def validate_skills_on_role_basis
    return if role.blank?

    if basic_user? && skills.length > 0
      errors.add(:skill_tokens, "Basic user is not allowed to add skills")
    end
        
    if business_user? && skills.length > 1
      errors.add(:skill_tokens, "Business user is allowed to add only one skills")
    end
  end

  def valid_postal_code
    # errors.add(:postal_code, "Postal code is invalid") unless zipcode
    true
  end

  def zipcode
    @zipcode ||= ZipCode.find_by_ZipCode(postal_code)
  end

  def get_latitude
    return latitude if latitude.present?
    return zipcode.Latitude.to_f if zipcode.present?
    return nil
  end

  def get_longitude
    return longitude if longitude.present?
    return zipcode.Longitude.to_f if zipcode.present?
    return nil
  end

  ######## Searchable block ########
  searchable do
    text :first_name, :as => :first_name_textp  
    text :last_name, :as => :last_name_textp 
    text :business_name, :as => :business_name_textp
    text :username, :as => :username_textp 
    text :email, :as => :email_textp  
    # text :skills do
    #   skills.map { |skill| skill.name }
    # end
    
    # text :search_skills , :as  => :user_skill do
    #   skills.map { |skill| skill.name }
    # end

    # text :search_offering_skills , :as  => :offering_skill do
    #   offering_skills.map { |skill| skill.name }
    # end

    text :skills, :as => :skill_textp do 
      skills.map(&:name).reject(&:blank?).join(" ") 
    end
    text :search_skills , :as  => :user_skill_textp do 
      skills.map(&:name).reject(&:blank?).join(" ") 
    end
    text :search_offering_skills , :as  => :offering_skill_textp do 
      offering_skills.map(&:name).reject(&:blank?).join(" ") 
    end
    
    # text :search_looking_for_skills , :as  => :looking_for_skill do
    #   looking_for_skills.map { |skill| skill.name }
    # end

    text :postal_code , :as => :user_postal_code
      
    latlon(:location) { Sunspot::Util::Coordinates.new(get_latitude, get_longitude) }
    string  :postal_code
    string  :role_name
    boolean  :is_paid do
        is_paid_user?
    end

    boolean  :barter

    float :rating_average
    integer :id
    string :skill_names, :multiple => true do
      skills.map { |skill| skill.name }
    end

    integer :referred_by_users_count do
      referred_by_users.size
    end
    integer :looking_for_skill_ids, :multiple => true
  end  

  def full_address
    [address_1, address_2, city, state_code, postal_code].select {|x| x.present?}.join(', ')
  end
 
  def self.find_users(query,page,lat, long)
    if lat.present? and long.present? 
       User.search(:include => [:photos, :skills, :barter_skills, :referred_by_users ]) do
        fulltext query do
          boost(4.0) { with(:role_name, Role::ROLES[:premium][:name]) }
          boost(2.0) { with(:role_name, Role::ROLES[:business][:name]) }
        end
        #with(:location).in_radius(zipcode.Latitude.to_f, zipcode.Longitude.to_f, RADIUS)
        #with(:is_paid , true)
        
        order_by_geodist(:location, lat, long)
        order_by :referred_by_users_count, :desc
        order_by :rating_average, :desc

        paginate :page => page || 1, :per_page => 10
       end.results
    else
       User.search(:include => [:photos, :skills, :barter_skills, :referred_by_users ]) do
        #with(:is_paid , true)
        order_by :referred_by_users_count, :desc
        order_by :rating_average, :desc
        fulltext query do
          boost(4.0) { with(:role_name, Role::ROLES[:premium][:name]) }
          boost(2.0) { with(:role_name, Role::ROLES[:business][:name]) }
        end

        paginate :page => page || 1, :per_page => 6
       end.results
    end 
  end
  
  def self.find_barters(query ,page,current_user)
    if current_user 

      skill_ids =current_user.skills.map(&:id)
      
      User.search do
        fulltext(query, :fields => [:search_offering_skills])
        with(:barter , true)
        with(:looking_for_skill_ids, skill_ids.blank? ? [0] : skill_ids )
        #with(:location).in_radius(current_user.zipcode.Latitude.to_f, current_user.zipcode.Longitude.to_f, RADIUS)
        #with(:is_paid , true)
        order_by :rating_average, :desc
        paginate :page => page || 1, :per_page => 6        
      end.results
    else
       User.search(:include => [:schedule]) do
        fulltext(query, :fields => [:search_offering_skills])
        #with(:is_paid , true)
        order_by :rating_average, :desc
        paginate :page => page || 1, :per_page => 6
       end.results
    end 
  end

  def self.find_referals(skill ,page,current_user)

    User.search(:include => [:schedule]) do
        #fulltext query
        #with(:location).in_radius(current_user.zipcode.Latitude.to_f, current_user.zipcode.Longitude.to_f, RADIUS)
        with(:skill_names, [skill])
        without(:id, [current_user.id])
        #with(:is_paid , true)
        order_by :rating_average, :desc
        paginate :page => page || 1, :per_page => 6
    end.results

  end


  # Reindex specific user only , instead of whole class.
  def reindex_user!
      Sunspot.index! self
  end

  def skill_tokens=(ids)
    self.skill_ids = ids.split(",")
  end

  def barter_skill_tokens=(ids)
    if self.barter  
      self.barter_skill_ids = ids.split(",")
    else
      self.barter_skill_ids = []
    end  
  end

  def offering_skill_tokens=(ids)
    self.skill_users.update_all(:offering_for_barter => false)
    
    if self.barter
      self.skill_users.where("skill_id IN (?)",ids.split(",")).update_all(:offering_for_barter => true) 
    end
  end

  def offering_skills
    self.skills.offering
  end
  
  def looking_for_skills
    self.barter_skills
  end

  def looking_for_skill_ids
    looking_for_skills.map(&:id)
  end

  def destroy_forwarded_token_if_present
    if !self.forwarded_token.blank?
      token = Token.find_by_value(self.forwarded_token)
      token.destroy
    end  
  end  

  def save_with_payment
    if is_paid_user?
      if payment_detail.process?(Role::ROLES[membership.to_sym][:price],billing_address) 
        add_role Role::ROLES[membership.to_sym][:name] 
        return true if save
      end      
    else
      add_role Role::ROLES[membership.to_sym][:name]  
      return true if save
    end

    return false 
  end 

  def full_name
    [first_name,last_name].reject(&:blank?).join(" ").try(:titleize)
  end

  def billing_address
    {
      :name     => full_name,
      :address1 => address_1,
      :city     => city,
      :state    => state_code,
      :country  => country_code,
      :zip      => postal_code
    }
  end

  def role?(name)
    if role
      role.name == name.to_s
    else
      membership == name.to_s
    end  
  end 

  def basic_user?
    role_name == Role::ROLES[:basic][:name]
  end 

  def business_user?
    role_name == Role::ROLES[:business][:name]
  end 

  def premium_user?
    role_name == Role::ROLES[:premium][:name]
  end
  
  def is_paid_user?
    role_name == Role::ROLES[:premium][:name]
  end 

  def add_role(name)
    self.role_id = Role.find_by_name(name).id
  end  

  def role_name
    @role_name ||= self.role.present? ? self.role.name : membership
  end

  def can_upgrade_role_to?(new_role)
      Role::ROLES[new_role.to_sym][:level] > Role::ROLES[self.role.name.to_sym][:level]
    rescue
      return false
  end

  def update_membership(new_role)
      if Role::ROLES[new_role.to_sym][:price] <= Role::ROLES[self.role.name.to_sym][:price]
        add_role(new_role)
        save(:validate => false)
      else
        errors.add(:base , "Membership could not be downgraded")
        return false
      end  
    rescue
      errors.add(:base , "Membership could not be downgraded")
      return false
  end


  #TODO Remove this manual code from here , It is only for testing purpose.
  def find_or_create_business_with_current_user(user)
      business = Business.find_or_create_by_employee_id_and_employer_id_and_title(self.id,user.id,"This is default job")
      #business.blank? ? Business.create(:employee_id => self.id, :employer_id =>user.id , :title => 'Test Business') : business
  end 

  def already_rated?(emp)
      business_ary = Business.where("employer_id = ? and employee_id = ?",self.id,emp.id)
      business_ary.blank? ? false : (Rate.where("rater_id = ? and rateable_id = ?",self.id,business_ary.first.id).blank? ? false : true)
  end
  #TODO 
  
  def business_waiting_for_feedback_or_rating_from(user) 
      business_done_with(user).select{|business| business.feedabck_pending? || business.rating_pending?(self.id) }
  end  

  def business_done_with(user)
      employee_businesses.where("employer_id = ?",user.id)
  end  
  

  def get_skills
      self.skills.map(&:name)
  end 

  def full_name_or_email
      name_or_email = if !self.name.blank?
                          self.name.try(:titleize)
                      else
                          self.email
                      end

      return name_or_email
  end 


  ########## Methods for Affiliate Programe ############ 

  ## New

  def create_affiliation
      affiliate = User.find_by_email(self.affiliate_email)
      reverse_affiliation = self.build_reverse_affiliation
      reverse_affiliation.affiliate_id = affiliate.id
      reverse_affiliation.reward_amount = self.reward_amount_to_affiliate
  end

  def add_affiliator_in_pr_list
      affiliate = User.find_by_email(self.affiliate_email)
      # Add user to affiliators pr list
      unless self.skills.blank?
        affiliate_referal = affiliate.referals.build
        affiliate_referal.referred_user = self
        affiliate_referal.skill = self.skills.first
        affiliate_referal.save
      end  
      # Add affiliator to users pr list
      unless affiliate.skills.blank?
        referal = self.referals.build
        referal.referred_user = affiliate
        referal.skill = affiliate.skills.first
        referal.save
      end  
  end

  def reward_amount_to_affiliate
      case self.role_name
        when Role::ROLES[:business][:name]
             return Affiliation::REFERRAL_AMOUNT[:first_tier][:business]
        when Role::ROLES[:premium][:name]
             return Affiliation::REFERRAL_AMOUNT[:first_tier][:premium]
        else
             return Affiliation::REFERRAL_AMOUNT[:first_tier][:basic]
      end
  end  

  def affiliated_by_someone?
    return false if affiliate_email.blank?
    User.where(email: affiliate_email).present?
  end

  def interested_to_join_affiliate_programe?
      return true # !is_affiliate.blank? && is_affiliate == "1"
  end

  def set_affiliate_token
      self.affiliate_token = generate_random_token
  end

  def generate_random_token
      chars = [*('A'..'Z'), *('a'..'z'), *(0..9)]
      str1 = (0..3).map {chars.sample}.join
      str2 = Digest::SHA1.hexdigest("--#{Time.now.to_s}--#{str1}--")[0,4]
      return (str1 + str2)
  end

  def is_affiliate?
      return !self.affiliate_token.blank? #|| self.w9_form_detail.blank?)
  end

  def send_email_to_affiliate
      UserMailer.send_promo_link_to_affiliate(self).deliver!
  end  

  def send_welcome_email
      # UserMailer.welcome_email(self).deliver!
  end  

  # Adaptive Payment API

  def pay_request 
      PaypalAdaptive::Request.new
  end

  # Mass Pay Request

  def mass_pay_api
      @api = PayPal::SDK::Merchant::API.new
  end


  def varify_paypal_email 
      verified_status_response = pay_request.get_verified_status({
                                :emailAddress => self.email,
                                :matchCriteria => "NONE" })

      if verified_status_response.success? 
         if verified_status_response["accountStatus"] == "VERIFIED"
            return true
         else
            self.errors.add(:base , "User's paypal account is not verified yet")
            return false
         end 
      else
          self.errors.add(:base , "User's email is not a valid paypal email")
          return false
      end  

  end

  def mass_pay_item
          items = [{
              :ReceiverEmail => self.email,
              :Amount => {
                  :currencyID => "USD",
                  :value => self.total_reward 
              }
          }]

          # If this is a second level affiliate then send some money
          # to his 1 tear affiliate as well
          items << {
                  :ReceiverEmail => self.affiliate.email,
                  :Amount => {
                      :currencyID => "USD",
                      :value => self.total_reward(:second_tier) 
                  }
              } if self.affiliate
      
      return items
  end    

  def payment_using_paypal   

      # Build request object
      @mass_pay = mass_pay_api.build_mass_pay({
          :ReceiverType => "EmailAddress",
          :MassPayItem => mass_pay_item
      })

      # Make API call & get response
      @mass_pay_response = @api.mass_pay(@mass_pay)

      # Access Response
      if @mass_pay_response.success?
          transaction = self.reward_transactions.build(:response =>@mass_pay_response.to_hash )
          transaction.first_level_reward = self.total_reward
          transaction.second_level_reward = self.affiliate.blank? ? 0.00 : self.total_reward(:second_tier)
          transaction.total_amount = transaction.second_level_reward + transaction.first_level_reward 
          transaction.save
          return true
      else
         self.errors.add(:base , @mass_pay_response.Errors)
         return false
      end

  end

  def total_reward_for_basic_users(tier = :first_tier)
    return (Affiliation::REFERRAL_AMOUNT[tier][:basic] * self.downline_members.pending_rewards.basic.size)
  end 

  def total_reward_for_business_users(tier = :first_tier) 
    return (Affiliation::REFERRAL_AMOUNT[tier][:business] * self.downline_members.pending_rewards.business.size)
  end
    
  def total_reward_for_premium_users(tier = :first_tier)
    return (Affiliation::REFERRAL_AMOUNT[tier][:premium] * self.downline_members.pending_rewards.premium.size )
  end 

  def total_reward(tier = :first_tier)
    return (total_reward_for_basic_users(tier) + total_reward_for_business_users(tier) + total_reward_for_premium_users(tier))
  end 

  def is_reward_amount_more_than_zero?
      self.total_reward > 0
  end  
  
    
  def rewarded_amount_by down_line_user
    self.affiliations.find_by_downline_member_id(down_line_user.id).reward_amount
  end
  
  def request_reward_for down_line_user
    self.affiliations.find_by_downline_member_id(down_line_user.id).request_reward
  end

  def eligible_for_requesting_reward?
    is_reward_amount_enough_to_request? && !is_reward_requested?
  end

  def is_reward_requested?
      return self.is_request_reward
  end

  def is_reward_amount_enough_to_request?
    total_reward > MINIMUM_REWARD_AMOUNT
  end

  def active?
    active
  end

  def activate!
    update_attribute(:active, true)
  end

  def deactivate!
    update_attribute(:active, false) unless is_admin?
  end

  def destroy!
    destroy unless is_admin?
  end

  def active_for_authentication?
    super && self.active?
  end

  def inactive_message
    return super unless self.confirmed?
    "Sorry, this account has been deactivated."
  end

  def set_invite_accepted
    invite = Invite.where(:token => invite_token).first
    invite.reciever = self    
    invite.accepted!
    # Forcefully add sender into pr list
    refer(invite_sender, invite.sender.skills.first, true)
  end

  def pending_invites
    Invite.pending.where(:email => email)
  end

  def pending_invites?
    pending_invites.present?
  end

  # Show senders of 'pending invites' on reciever login.
  # and give options whether he want to add any one of
  #these into his pr or not.Then update all invites
  # to viewed
  def update_all_invites_to_viewed
    pending_invites.update_all(:status => 'viewed')
  end

end
