class Affiliation < ActiveRecord::Base
  # attr_accessible :title, :body

   belongs_to :downline_member , :class_name => "User"
   belongs_to :affiliate , :class_name => "User"

   validates :downline_member_id , :presence => true
   validates :affiliate_id , :presence => true
   

   #REFERRAL_AMOUNT = { :first_tier => {:basic => 0.0, :business => 20.00 , :premium => 40.00} , :second_tier => {:basic => 0.0, :business => 10.00 , :premium => 20.00} } 
   
   # For test
   REFERRAL_AMOUNT = { :first_tier => {:basic => 0.0, :business => 3.00 , :premium => 4.00} , :second_tier => {:basic => 0.0, :business => 1.00 , :premium => 2.00} }


end
