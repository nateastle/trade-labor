class W9FormDetail < ActiveRecord::Base
  attr_accessible  :country_code ,  :address1  ,:address2, :city , :pin  , :state_code , :name , :business_name , :identification_type , :identification_number

  IDENTIFICATION_TYPE = { :ssn => "SSN" , :tin => "TIN"}
   
  validates :name  , :country_code , :address1 , :city , :pin , :state_code , :identification_number , :presence => true
  validates :identification_type ,:inclusion => { :in => IDENTIFICATION_TYPE.values ,:message => "Should be selected"} 

  attr_accessor :other_info_str


  belongs_to :user

  serialize :other_info, Hash

  # Name (as shown on your income tax return) ,
  # Business name/disregard entity name, if different from above 
  
  # Check appropriate box for federal tax classification: ....., Exempt Payee

  # Address (number, street, and apt. or suite no.) 
  # Requester's name and address (optional) 
  # City, state, and ZIP code 
  # List account number(s) here (optional) 

  # Social security number 
  # Employee identification number 


  # def  validate_ssn_and_ein
  #     if ssn.blank? && ein.blank?
  #       # Reove tax id from signup by migration
  #         errors.add(:base, 'Please fill either your SSN or EIN or both')
  #     end  
  #  end 
end
