class UserImport < ActiveRecord::Base
  # attr_accessible :title, :body
  mount_uploader :csv, AttachmentUploader

  attr_accessible :csv

  after_create :enqueue_import

  def enqueue_import
  	Delayed::Job.enqueue(ParseCsvJob.new(self.id))
  end

end
