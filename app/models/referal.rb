class Referal < ActiveRecord::Base
  # attr_accessible :user_id, :referred_user_id , :position
  belongs_to :user
  belongs_to :skill
  belongs_to :referred_skill, :class_name => "Skill", :foreign_key => :skill_id # Alias of skill
  
  belongs_to :referred_user , :class_name => "User"
  acts_as_sortable
  acts_as_list scope: :user

  after_create :insert_at_top_position
  after_destroy :reindex_referred_user

  def insert_at_top_position
  	self.move_to_top
  end

  def reindex_referred_user
    Sunspot.index! [self.referred_user] if self.referred_user
  end

  def add_refered_user!(user)
    self.referred_user = user
    self.save!
    self.move_to_top
  end
end
