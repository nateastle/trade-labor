class EmailVerificationLog < ActiveRecord::Base
  attr_accessible :user_id, :email, :success, :valid_email, :error, :note
end
