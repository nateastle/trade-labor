class FailedUserImport < ActiveRecord::Base
  attr_accessible :email, :validation_errors, :emails, :categories, :headline, :website, :phone_number,
  :city, :address, :state, :zip_code, :country, :latitude, :longitude

  serialize :data, Hash

  def self.create_or_update(user, row)
    record = where(email: user.email).first || new(email: user.email)
    begin
      record.validation_errors = user.errors.full_messages.uniq.join(", ")
      record.data = row.to_hash
      record.attributes = row
      record.save
    rescue Exception => e
      record = where(email: user.email).first || new(email: user.email)
      record.emails = row['emails']
      record.validation_errors = "Exception in create_or_update: #{e.message}"
      record.save
    end  
  end

  def retry
    user = User.import(attributes)
    user.failed_import? ? update_validation_errors(user) : destroy
  end

  def update_validation_errors(user)
    update_attribute(:validation_errors, user.errors.full_messages.join(", "))
  end

end