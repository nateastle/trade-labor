class Skill < ActiveRecord::Base
 require 'rubygems'
 require 'roo' 

  attr_accessible :name
  has_many :skill_users, :dependent => :destroy
  has_many :referals, :dependent => :destroy
  has_many :users , :through => :skill_users, :uniq => true

  validates :name, :presence => true 
  validates_uniqueness_of :name

  scope :offering , where("skill_users.offering_for_barter = ?",true) 

  # after_save :reindex_users!
  # # Reindex corresponding user only , instead of whole class.
  # def reindex_users!
  #     Sunspot.index! self.users
  # end  

  searchable do
     text :name, :as => :user_skill
     integer :id
  end 

  def merge!(skill_ids)
    skill_ids = skill_ids.compact.uniq - [self.id]
    to_skills = Skill.includes(:skill_users, :referals).where(id: skill_ids)
      
    skill_user_ids = to_skills.map(&:skill_user_ids).flatten.compact.uniq
    referal_ids = to_skills.map(&:referal_ids).flatten.compact.uniq

    skill_users = SkillUser.where(id: skill_user_ids)
    referals = Referal.where(id: referal_ids)

    begin
      Skill.transaction do
        referals.update_all(skill_id: self.id)
        skill_users.update_all(skill_id: self.id)
        to_skills.delete_all
      end
      return true
    rescue Exception => e
      return false
    end  
  end

  def apply_merge(skill_ids_to_merge)
    return false if skill_ids_to_merge.blank?
    return merge!(skill_ids_to_merge)
  end

  def self.import(path = Rails.root.join('lib','data/skills.xlsx'))
     unless path.blank?
      Skill.delete_all
      file =  File.open(path,"r")
      spreadsheet = open_spreadsheet(file)
      header = spreadsheet.row(1)
        (2..spreadsheet.last_row).each do |i|
          row = Hash[[header.map!(&:downcase), spreadsheet.row(i)].transpose]
          skill = Skill.new
          skill.attributes = row.to_hash.slice(*accessible_attributes)
          skill.save!
        end
        file.close
     end
  end	

  def self.open_spreadsheet(file)
  
    case File.extname(File.basename(file.path))
    #case File.extname(file.original_filename)
    when ".csv" then Csv.new(file.path, nil, :ignore)
    when ".xls" then Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Excelx.new(file.path, nil, :ignore)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

  def self.insert_and_return(names)
    return nil if names.blank?
    skills = []
    names.split(/[;:,]/i).map(&:squish).each do |name|
      name = name.try(:downcase).try(:titleize)
      skills << Skill.find_or_create_by_name(name)
    end
    skills
  end

end
