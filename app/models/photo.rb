class Photo < ActiveRecord::Base
  attr_accessible :name, :user_id , :is_profile_photo
  belongs_to :user

  mount_uploader :name, PhotoFileUploader

  scope :profile_photo , where("is_profile_photo = ?",true)
  scope :all_except, lambda{|photo| photo ? {:conditions => ["photos.id != ?", photo.id]} : {} }

  validates_presence_of :name, :message => "Please choose the file."

  after_save :update_profile_picture

  def update_profile_picture
    self.user.photos.all_except(self).update_all(:is_profile_photo => false) if self.is_profile_photo
  end

  def is_profile_picture?
   is_profile_photo
  end 

  def is_not_profile_picture?
   !is_profile_photo
  end 

  def set_as_profile_photo
    update_attribute(:is_profile_photo, true)
  end

end
