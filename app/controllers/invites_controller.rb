class InvitesController < ApplicationController
	before_filter :authenticate_user!, :except => [:decline]

	def decline
		@invite = Invite.where(:token => params[:token]).first

		if @invite
			if @invite.initiated?
				@invite.decline
				@invite.save(:validate => false)
				redirect_to root_path, :notice => "You have declined the invite successfully"
			else
				if @invite.decline? || @invite.accepted?
					redirect_to root_path, :alert => "You have already #{@invite.status} the invitation"
				else
					redirect_to root_path, :alert => "Token is not valid"
				end
			end	
		else
			redirect_to root_path, :alert => "Invite does not exist"
		end	
	end

	def new
		@invite = current_user.sent_invites.build
	end

	def create
		@invite = current_user.sent_invites.build(params[:invite])
		if @invite.save
			redirect_to root_path, :notice => "Invite has been sent successfully"
		else		
			render :new
		end	
	end

	def preview
		render :layout => false
	end

end
