class AffiliatesController < ApplicationController

  before_filter :authenticate_user!
  before_filter :check_for_w9_form_detail , :only => [:affiliate]

  #skip_before_filter :w9_form_info_for_affiliate, :only => :subregion_options

  def w9_form_detail
    @w9_form_detail = current_user.w9_form_detail || current_user.build_w9_form_detail
  end

  def update_w9_form_detail
    @w9_form_detail = current_user.w9_form_detail
    @w9_form_detail.other_info = params[:other_info_str]

    if @w9_form_detail.update_attributes(params[:w9_form_detail])
      flash[:notice] = "W9 detail has been updated successfully"
    redirect_to w9_form_path
    end
  end

  def create_w9_form_detail

    @w9_form_detail = current_user.build_w9_form_detail(params[:w9_form_detail])
    @w9_form_detail.other_info = params[:other_info_str]

    if @w9_form_detail.save
      flash[:notice] = "Congratulations for joining the Affiliate program , You will receive an email with Affiliate Link in a few minutes "
      current_user.send_email_to_affiliate
      redirect_to root_path
    else
      render :w9_form_detail
    end
  end

  def affiliate
  end


  def check_for_w9_form_detail
    if  current_user.w9_form_detail.blank?
     flash[:notice] = "Join the affiliate program by completing W9 detail"
     redirect_to w9_form_path and return
    end
  end

end
