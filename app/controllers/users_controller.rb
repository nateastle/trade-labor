class UsersController < ApplicationController

  before_filter :authenticate_user! , :only => [:save_locations, :rate,:post_feedback]
  before_filter :find_user , :only => [:show, :add_referal]
  before_filter :find_business , :only => [:show]


  # def express_checkout
  #   response = EXPRESS_GATEWAY.setup_purchase(Role::ROLES[:premium][:price]*100,
  #     ip: request.remote_ip,
  #     return_url: new_user_registration_url,
  #     cancel_return_url: new_user_registration_url,
  #     currency: "USD",
  #     allow_guest_checkout: true,
  #     items: [{name: "Premium signup", description: "Signup as premium user", quantity: "1", amount: Role::ROLES[:premium][:price]*100}]
  #   )
  #   redirect_to EXPRESS_GATEWAY.redirect_url_for(response.token)
  # end

  def save_locations
   temp_locations = current_user.save_temp_locations(location_params)

   if TempLocation.saved? temp_locations
     @res = {:success => true, :message => 'Success, now proceed to payment' }
   else
    invalid_loc = temp_locations.reject(&:valid?).first
    message = invalid_loc.present? ? invalid_loc.errors.full_messages.uniq.join(", ") : 'Locations could not be saved'
    @res = {:success => false, :message => message }
   end
  end

  def edit_referal
    @user = User.where('id = ?', params[:id]).first
    @response = {}
    if current_user.can_refer?(@user)
      if @user.skills.size > 1
        @response[:success] = true
        @response[:multiple_skills] = true
        @skills = @user.skills# - current_user.referred_skills # Only show those skills which are not refred yet
        # Show drop down and ask user, for whihc skill you
        # want to refer this user

        # If from drop down, you selected a skill for which referal already exist then
        # Then, whether you want to remove existing one or cancel the process
      else
        @skill = @user.skills.first
        if current_user.is_referal_already_exist_with_skill?(@skill)
          # Ask user, whether he want to cancel or proceed
          # If he proceed then remove the existing and add
          # This user at top.
          @response[:success] = true
          @response[:already_exist] = true
        else
          res = current_user.refer(@user, @skill)
          @response[:success] = res[:success]
          @response[:new_referal] = true
          # Add into pr and replace button with success message.
        end
      end
    else
      @response = {:success => false, :message => 'This user can not be added to your prefered referal list'}
    end
  end

  def update_referal
    if params[:already_exist].present? # Replace existing one
      @existing_referal = current_user.referals.where("skill_id = ?",params[:skill_id]).first
      @new_user_to_refer = User.where("id = ?",params[:user_id]).first
      if current_user.can_refer?(@new_user_to_refer)
        @existing_referal.add_refered_user!(@new_user_to_refer)
        @responce = {:success => true, :message => "User has been added to your prefered referal list"}
      else
        @responce = {:success => false, :message => "User could not be added to pr list"}
      end  
    elsif params[:multiple_skills].present? # User have mutiple skills
      if current_user.referred_skill_ids.include?(params[:skill_id])
        @responce = {:success => false, :message => "Referal already exist for this skill"}
      else
        @user = User.where("id = ?",params[:user_id]).first
        if current_user.can_refer?(@user)
          @skill = @user.skills.where("skills.id = ?",params[:skill_id]).first
          res = current_user.refer(@user, @skill)
          @responce = {:success => res[:success], :message => res[:message]}
        else
          @responce = {:success => false, :message => "User could not be added to pr list"}
        end  
      end  
    else
      @responce = {:success => false, :message => "Please try later"}
    end
  end

  def express_payment

    if params[:token].blank?
      flash[:alert] = "Invalid token" 
      redirect_to new_user_registration_url and return
    end  

    #params: {"token"=>"", "PayerID"=>""}
    temp_user = TempUser.find(session[:temp_user_id])
    user = temp_user.initialize_user
    user.terms_of_service = true
    payment_detail = user.payment_detail
    payment_detail.express_token = params[:token]    

    # Here set password, password_confirmation & term_of_service
    if payment_detail.valid? && user.save_with_payment
      session[:temp_user_id] = nil
      flash[:notice] = "You have registered successfully, please login now"  
      redirect_to new_user_session_path
    else
      session[:temp_user] = nil
      flash[:alert] = "Invalid token"
      new_user_registration_url
    end

  end

  def show
    @rated_business_array = @user.employee_businesses.includes(:employer).rated  || [] 
    @rated_business_array = @rated_business_array.select{|business| business.employer.present? }.paginate(:page => params[:rating_page] || 1, :per_page => 3)
    
    @referals = @user.referals.includes([:referred_user,:skill]).order("referals.position ASC").paginate(:page => params[:referal_page] || 1, :per_page => 8)
    @locations = @user.locations
    # @referred_users = @referals.map(&:referred_user)

    @referred_skill_names = @referals.map {|referal| referal.skill.try(:name)}
    if current_user && !current_user.is?(@user)
      @refered_skill_ids = current_user.refered_skill_ids_for(@user)
    end  
    respond_to do |format|
      format.html {  render :layout => 'custom_profile' }
      format.js { }
    end
   
  end

  def rate
    @business = Business.find(params[:id])
    @business.rate(params[:stars], current_user, params[:dimension])
  end
  
  def post_feedback
    @business = current_user.employer_businesses.find_by_id(params[:business_id])
    @business.submit_feedback!(params[:business][:feedback_text])

    respond_to do |format|
      format.html { redirect_to user_path(@business.employee.username), notice: "Feedback submitted successfully" }
      format.js { }
    end
    
  end
  
  def report
    @downline_members = current_user.downline_members.order("created_at DESC")
  end
  
  def request_pay
    if current_user.eligible_for_requesting_reward?
      current_user.is_request_reward = true
      if current_user.save and UserMailer.send_payment_request(current_user).deliver!
        flash[:notice] = "Payment Request submitted successfully"
      else
        flash[:error] = "There is an error while submitting payment request"
      end  
    else
      flash[:error] = "Either amount is less than #{User::MINIMUM_REWARD_AMOUNT} or already requested"
    end  
    redirect_to report_user_path(current_user)
  end

private 

  # def find_business
  #   @user = User.find(params[:id])
  #   @business = @user.find_or_create_business_with_current_user(current_user) if current_user && (current_user.id != @user.id)    
  # end 
  def find_user
    @user = User.find_by_username(params[:username])
  end

  def find_business
    if !@user.blank?
      if !current_user.blank? && !current_user.is?(@user) && !current_user.is_admin?
          @business = @user.business_waiting_for_feedback_or_rating_from(current_user).first 
          ## TODO :: Remove this once we have done with hiring funtionlaity.
          @business = @user.find_or_create_business_with_current_user(current_user) if @business.blank?
          ## TODO ::
      end 
    else
      redirect_to root_path , :alert => "#{params[:username]} user does not exist"
    end  
  end 

  def location_params
    @location_params ||= params["locations"].values.map{ |loc| loc["user"]["locations_attributes"] }.map(&:values).flatten rescue []
  end
end
