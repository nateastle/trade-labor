class SessionsController < Devise::SessionsController

  before_filter :check_admin, only: [:create]
  after_filter :set_flash_message_after_login, only: [:create]

  def check_admin
      if !params[:admin_login].blank? && params[:admin_login] == "1"
          
          
        if params[:user][:email].blank? || params[:user][:password].blank?
            flash[:alert] = "Invalid email or password." 
            redirect_to admin_login_path  
        else    
          user = User.find_by_email(params[:user][:email])
          if user.blank? || !user.is_admin? 
            signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(:user))
            flash[:alert] = "You are not admin so please login as user" 
            redirect_to new_user_session_path 
          end
        end  
      else
          user = User.find_by_email(params[:user][:email])
          if user && user.is_admin?
              signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(:user))
              flash[:alert] = "You are admin , Please login from here" 
              redirect_to admin_login_path 
          end
      end      
  end 
  
  protected
    
  def set_flash_message_after_login
      flash[:notice] = current_user.is_admin? ? "WelCome Admin" : "Signed in successfully" if current_user
  end

end 