class TempLocationsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :validate_temp_locations, except: [:destroy]
  before_filter :find_temp_location, only: [:destroy]

  def index
  end 

  def destroy
    @temp_location.destroy
    @temp_locations = current_user.temp_locations
  end 

  private

  def validate_temp_locations
    @temp_locations = current_user.temp_locations.order("created_at DESC")
    redirect_to root_path, alert: 'No any new location added yet' if @temp_locations.blank?
  end

  def find_temp_location
  	@temp_location = current_user.temp_locations.where(id: params[:id]).first
  end
end
