class BartersController < ApplicationController

  before_filter :authenticate_user! 

  def search
    @users = User.find_barters(params[:search] ,params[:page] ,current_user)
    respond_to do |format|
        format.html {render "index" }
        format.js
    end
  end  

end
