class ZipcodesController < ApplicationController
	def index
	  @zip_code = ZipCode.where(["\"ZipCode\" like ?", "#{params[:query]}%"])

	  #ZipCode.find(params[:id])
	  #@zipcodes = Zipcodes.order(:zip_code).where("zip_code like ?", "%#{params[:term]}%")
	  zip_codes =  @zip_code.map{|zip_code|  zip_code.ZipCode }
	  render json: {  query: params[:query] ,suggestions: zip_codes.uniq ,data: zip_codes }
	end

	def zip
		@zip_codes = ZipCode.where(:ZipCode => params[:zip]).select('DISTINCT "CityAliasName" , "State"')
	  	@state = @zip_codes.map(&:State).uniq.first
	  	@postal_code_input_id = params[:postal_code_input_id] || 'user_postal_code'
	  	#render json: @zip_code
	end
end
