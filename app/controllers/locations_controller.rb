class LocationsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :find_location, only: [:edit, :update, :destroy]

  def sample_csv
    #sample_csv_locations_path
    send_file(
      "#{Rails.root}/lib/data/locations_sample.csv",
      filename: "locations_sample.csv",
      type: "text/csv"
    )
  end

  def index
    @locations = current_user.locations.order("created_at DESC")
  end 

  def update
    if @location.update_attributes(params[:location])
      redirect_to locations_path, notice: 'Location updated successfully'
    else
      render :edit
    end
  end  

  def destroy
    @location.destroy
  end

  def temp_import
    current_user.temp_locations.destroy_all
    current_user.locations_csv =  params[:user][:locations_csv]
    @res = current_user.import_temp_locations!
    #@temp_locations = @res[:temp_locations] 
  end

  def process_payment
    @res = Location.pay_and_save(current_user, params[:payment], request.remote_ip)
  end

  def express_checkout
    @temp_locations_count = current_user.temp_locations.count
    if express_checkout?
      if @temp_locations_count > 0
        redirect_to EXPRESS_GATEWAY.redirect_url_for(express_checkout_token)
      else
        redirect_to temp_locations, alert: 'No location added yet'
      end  
    else
      redirect_to temp_locations, alert: 'Not a valid request'
    end  
  end

  def express_payment
    if params[:token].blank?
      flash[:alert] = "Invalid token" 
      redirect_to temp_locations_url and return
    end

    @res = Location.pay_and_save(current_user, {express_token: params[:token]} , request.remote_ip, true)

    if @res[:success]
      redirect_to locations_path, notice: @res[:message]
    else
      redirect_to temp_locations_path, alert: @res[:message]
    end

  end

  private

  def find_location
    @location = current_user.locations.where(id: params[:id]).first
  end


  def express_checkout?
    params[:express_checkout].present?
  end

  def express_checkout_token
    response = EXPRESS_GATEWAY.setup_purchase(@temp_locations_count*100,
      ip: request.remote_ip,
      return_url: express_payment_locations_url,
      cancel_return_url: temp_locations_url,
      currency: "USD",
      allow_guest_checkout: true,
      items: [{name: "Add multiple locations", description: "Adding multiple locations", quantity: '1', amount: @temp_locations_count*100}]
    )

    return response.token    
  end

end
