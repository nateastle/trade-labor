class SearchesController < ApplicationController

  #before_filter :authenticate_user! 

  before_filter :find_lat_long , :only => [:search]
  
  def search
    @users = User.find_users(params[:search] ,params[:page] ,@lat, @long)

    referred_user_ids = @users.select{|user| user.referred_by_users.size > 0 }.map(&:id)

    @referrals = Referal.where(referred_user_id: referred_user_ids).includes(:user, :referred_user).paginate(:page => params[:page] || 1, :per_page => 10)

    @current_user_skill_ids = current_user.skill_ids if current_user

    respond_to do |format|
      format.html {render "welcome/index" }
      format.js
    end      
  end  

  def suggestions
    skills = Skill.search do
      fulltext params[:query]
      paginate :page => 1, :per_page => 500
    end.results.map(&:name)

    render json: {  query: params[:query] ,suggestions: skills ,data: skills }
  end 

  def find_lat_long
    @lat, @long = if current_user
      [current_user.get_latitude, current_user.get_longitude]
    elsif params[:zipcode]
      zipcode = ZipCode.find_by_ZipCode(params[:zipcode])
      [zipcode.Latitude, zipcode.Longitude]
    end
  end

end
