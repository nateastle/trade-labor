class ReferalsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :find_user_to_refer , :only => [:create]
  before_filter :find_skill , :only => [:create]

  def index
    @referals = current_user.referals.includes([:referred_user , :skill])
  end

  def search
    @skill_name = params[:skill]
    @already_referred_users = current_user.referred_users_for(@skill_name)
    @users = User.find_referals(params[:skill] ,params[:page] ,current_user)
  end 

  def create
    @response = current_user.refer(@user, @skill)
    @referal = @response[:referal] if @response[:success]
  end 

  def move
    @referal = current_user.referals.find(params[:id])
    @referal.move_to! params[:position]
  end

  def remove
    @referal = current_user.referals.find(params[:id])
    @referal.remove_from_list
    @referal.destroy
  end

 private

  def find_user_to_refer  
    @user = User.find(params["user_id"])
  end 

  def find_skill
    @skill = Skill.find_by_name(params["skill_name"])  
  end 
end
