class ApplicationController < ActionController::Base
  protect_from_forgery

  include ::SslRequirement
  ssl_exceptions

  before_filter :w9_form_info_for_affiliate ,if: :current_user
  before_filter :store_location
  before_filter :check_host_to_redirect
  before_filter :validate_user!
  before_filter :update_claim

  helper_method :is_admin_signed_in?
  helper_method :current_user_is_admin?

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end

  def update_claim
    if user_signed_in?
      current_user.claim! unless current_user.claimed?
    end
  end

  def validate_user!
    if user_signed_in? && !current_user.valid? && !request.xhr?
      if !devise_controller? && request.path != "/membership"
        redirect_to edit_user_registration_path, alert: "#{current_user.errors.full_messages.join(', ')}" 
      end  
    end
  end

  def check_host_to_redirect
      if request.host =~ /tradelabor.net/
          
         token = Token.create

         if request.host_with_port =~ /:/
           redirect_to "//www.riszil.com:#{request.port}/users/sign_up?forwarded_token=#{token.value}", :status => 301
         else
           redirect_to "//www.riszil.com.com/users/sign_up?forwarded_token=#{token.value}", :status => 301 
         end
      end
  end  

  def w9_form_info_for_affiliate
  		if current_user.is_affiliate? && current_user.is_reward_requested? && current_user.w9_form_detail.blank? && (request.path != "/w9_form") && (request.path != "/users/sign_out")
  			flash[:notice] = "Great !! You are intrested in joining the Affiliate program , Please complete the w9 form deatil"
  			redirect_to w9_form_path
  		end
  end

  def after_sign_in_path_for(resource)
    url = session[:url_to_redirect] || session[:previous_url] || (current_user_is_admin? ? admin_root_path  : root_path)
    session[:url_to_redirect] = nil
    session[:previous_url]    = nil
    url
  end

  def after_sign_up_path_for(resource)
    url = session[:url_to_redirect] || session[:previous_url] || (current_user_is_admin? ? admin_root_path  : root_path)
    session[:url_to_redirect] = nil
    session[:previous_url]    = nil    
    url
  end

  def is_admin_signed_in?
      return ( !current_user.blank? && current_user.is_admin? )
  end

  def current_user_is_admin?
      current_user.is_admin?
  end

  def after_sign_out_path_for(resource_or_scope)
      current_user.is_admin? ? admin_login_path : root_path
  end

  def store_location
    return unless request.get? 
    
    if current_user.blank? && (request.path == "/users/sign_in" &&
        params[:user_url].present? )
      session[:url_to_redirect] = params[:user_url]
    end

    if current_user.blank? && (request.path != "/users/sign_in" &&
        request.path != "/users/sign_up" &&
        request.path != "/users/password/new" &&
        request.path != "/users/password/edit" &&
        request.path != "/users/confirmation" &&
        request.path != "/users/sign_out" &&
        request.path != "/admin/login" &&
        request.path != "/express_payment" &&
        !request.xhr?) # don't store ajax calls
      session[:previous_url] = request.fullpath 
    end
  end
  
  protected

  def ssl_required?
    if request.host =~ /tradelabor.net/
            return false
    else
            return true
    end
  end

end
