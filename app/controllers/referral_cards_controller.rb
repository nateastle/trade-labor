class ReferralCardsController < ApplicationController
  before_filter :authenticate_user!
  
  #layout 'pdf'
  # def show
  #   @cards_count = 10
  #   @cards_class = 'ten_sheet'    
  # end

  def print
    @cards_count = 4
    @cards_class = 'four_sheet'
    @user = current_user 
    
    if params[:size] == "ten_sheet"
      @cards_count = 10
      @cards_class = 'ten_sheet'
    end

    if params[:download]
      send(:download)
    elsif params[:preview]
      send(:preview)
    else 
      send(:send_by_email)
    end
  end

  private

  def download
    @writing_pdf = true

    render pdf:      'referral_card',
       disposition:  'attachment', 
       template:     'referral_cards/card',
       layout:       'pdf.html'              
  end

  def preview
    @writing_pdf = false
  end

  def send_by_email
    UserMailer.send_referral_card(@user, @cards_count, @cards_class).deliver!
  end

end
