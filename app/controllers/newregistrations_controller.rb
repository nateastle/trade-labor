class NewregistrationsController < Devise::RegistrationsController

  before_filter :find_affiliate_if_affiliated , :only => [:new]
  before_filter :set_affiliate_if_affiliated , :only => [:create]

  layout 'register' , :only => [:new,:create]

  def new
    build_resource({})
    resource.add_role(params["plan"] || 'premium')
    resource.invite_token = params[:invite_token] if params[:invite_token].present?
    yield resource if block_given?
    respond_with self.resource        
  end

  def create    

    resource.ip_address = request.remote_ip
    
    if resource.is_paid_user? && !express_checkout?
      resource.payment_detail.ip_address = resource.ip_address
    else
      resource.payment_detail = nil  
    end  
    
    if resource.valid?
      if express_checkout?
        temp_user = resource.create_temp_user
        session[:temp_user_id] = temp_user.id
        redirect_to EXPRESS_GATEWAY.redirect_url_for(express_checkout_token)
      else  
        if  resource.save_with_payment
          if resource.active_for_authentication?
            set_flash_message :notice, :signed_up if is_navigational_format?
            sign_up(resource_name, resource)
            redirect_to root_path(:from_signup => true)
          else
            set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_navigational_format?
            expire_session_data_after_sign_in!
            respond_with resource, :location => after_inactive_sign_up_path_for(resource)
          end
        else
            clean_up_passwords resource
            render :new
        end
      end    
    else
      resource.build_payment_detail if resource.payment_detail.blank?
      clean_up_passwords resource
      render :new    
    end  
  end 

  def edit
    resource.locations.build if resource.locations.persisted.blank?     
  end

  def update
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

    if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
      params[:user].delete(:current_password)
    end

    if update_resource(resource, account_update_params)
      if is_navigational_format?
        flash_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ?
          :update_needs_confirmation : :updated
        set_flash_message :notice, flash_key
      end
      sign_in resource_name, resource, :bypass => true
      redirect_to user_path(resource.username)
    else
      clean_up_passwords resource
      render :edit
    end
  end
  
  def find_affiliate_if_affiliated  
      if !params[:forwarded_token].blank?
          token = Token.find_by_value(params[:forwarded_token]) 
          if token.blank?
            redirect_to new_user_registration_path , :alert => "Forwarded token is not valid"
          else
            @affiliate_email = "david@dynamicsalesinnovations.com"
          end  
      else
          affiliate = User.find_by_affiliate_token( params["affiliate_token"] || cookies[:aff_tag] || request.env['affiliate.tag']) if (!params["affiliate_token"].blank? || !cookies[:aff_tag].blank? || !request.env['affiliate.tag'].blank?)
          @affiliate_email = cookies[:affiliate_email] = affiliate.email if affiliate    
      end
  end  

  def set_affiliate_if_affiliated
      
      if params[:forwarded_token].blank?
         resource = build_resource(sign_up_params)
         affiliate = User.find_by_email(resource.affiliate_email) if !resource.affiliate_email.blank?
         affiliate = User.find_by_affiliate_token(cookies[:aff_tag] || request.env['affiliate.tag']) if affiliate.blank? && ( !cookies[:aff_tag].blank? || !request.env['affiliate.tag'].blank?)
         resource.affiliate_email = affiliate.email if affiliate
      else
         token = Token.find_by_value(params[:forwarded_token]) 
         if !token.blank?
            resource = build_resource(sign_up_params)
            affiliate = User.find_by_email("david@dynamicsalesinnovations.com")
            if affiliate
              resource.affiliate_email = affiliate.email 
              resource.forwarded_token = token.value 
              resource.membership = Role::ROLES[:premium][:name]
            end  
         else
            redirect_to new_user_registration_path , :alert => "Forwarded token is not valid"
         end 
      end

      #resource.membership = Role::ROLES[:premium][:name]  
  end

  def express_checkout?
    !params[:express_checkout].blank? && params[:user][:payment_detail_attributes].blank?
  end

  def express_checkout_token
    response = EXPRESS_GATEWAY.setup_purchase(Role::ROLES[:premium][:price]*100,
      ip: request.remote_ip,
      return_url: express_payment_url,
      cancel_return_url: new_user_registration_url,
      currency: "USD",
      allow_guest_checkout: true,
      items: [{name: "Premium signup", description: "Signup as premium user", quantity: "1", amount: Role::ROLES[:premium][:price]*100}]
    )

    return response.token    
  end

  protected

  def update_resource(resource, params)
    if params[:password].blank? && params[:password_confirmation].blank?
      resource.update_without_password(params)
    else
     resource.update_without_password(params) 
     #super
    end
  end
end 