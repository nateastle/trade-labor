class PasswordsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :find_user

  def update
    if @user.update_with_password(params[:user])
      sign_in(@user, :bypass => true)
      redirect_to root_path, :notice => "Password updated successfully"
    else
      render :edit
    end
  end

  private

  def find_user 
    @user = User.find(current_user.id)
  end
end
