class MembershipsController < ApplicationController

  before_filter :authenticate_user! , :except => [:new]

  layout 'register'

  def new
    redirect_to membership_path if current_user
  end

  def change
    if params[:downgrade]
      if current_user.update_membership(params[:membership])
        flash[:notice] = "Membership downgraded successfully"   
      else
        flash[:alert] = current_user.errors.full_messages.join(",")  
      end
    elsif params[:upgrade] && current_user.can_upgrade_role_to?(params[:membership])
      #TODO : Just remove this condition if you want payment for business mebrship too
      if params[:membership] ==  Role::ROLES[:business][:name]
        current_user.update_membership(params[:membership])
      else  
        @payment_detail = current_user.build_payment_detail(:upgrading_to => params[:membership])
      end  
    end 
  end

  def upgrade
    @payment_detail = current_user.build_payment_detail(params[:payment_detail])
    current_user.membership = @payment_detail.upgrading_to
    @res = if current_user.save_with_payment
      {:success => true , :message => "Membership upgraded successfully"}
    else
      {:success => false , :message => "Membership could not be upgraded"}
    end
  end

  # While upgrading to paid plan
  def express_checkout
    redirect_to membership_path, alert: 'You are already a premium member' if current_user.premium_user? 
    redirect_to membership_path, alert: 'Not a valid request to upgrade membership plan' if params[:express_checkout].blank?

    redirect_to EXPRESS_GATEWAY.redirect_url_for(express_checkout_token)
  end

  def process_payment
    #redirect_to membership_path, alert: 'Invalid token' if params[:token].blank?
    # "token"=>"EC-02N40293JF5951930", 
    # "PayerID"=>"PNY8Q686F25LG"

    payment_detail = current_user.build_payment_detail
    payment_detail.express_token = params[:token]    
    current_user.membership = Role::ROLES[:premium][:name]

    if payment_detail.valid? && current_user.save_with_payment
      flash[:notice] = "You have successfully upgraded to #{current_user.membership} membership plan"  
      redirect_to membership_path
    else
      flash[:alert] = "Payment could not be completed"
      redirect_to membership_path
    end
  end


  private

  def express_checkout_token
    response = EXPRESS_GATEWAY.setup_purchase(Role::ROLES[:premium][:price]*100,
      ip: request.remote_ip,
      return_url: process_payment_membership_url,
      cancel_return_url: membership_url,
      currency: "USD",
      allow_guest_checkout: true,
      items: [{name: "Premium signup", description: "Signup as premium user", quantity: "1", amount: Role::ROLES[:premium][:price]*100}]
    )

    return response.token    
  end  

end
