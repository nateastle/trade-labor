class WelcomeController < ApplicationController
  
  RADIUS = 25
  
  skip_before_filter :w9_form_info_for_affiliate, :only => :subregion_options

  def robots                                                                                                                                      
    robots = File.read(Rails.root + "config/robots.#{Rails.env}.txt")
    render :text => robots, :layout => false, :content_type => "text/plain"
  end

  def fbhome
    Rails.logger.info "Params from fb---#{params.inspect}"
    redirect_to root_url, notice: 'Tab has been added to your facebook page successfully'
  end  

  def about_us
  end  

  def index
    if !params[:username].blank?
      @user = User.find_by_username(params[:username])
      referals = @user.referals.includes([:skill]).order("referals.position ASC").limit(10)  
      @referred_skill_names = referals.map {|referal| referal.skill.name}
      flash[:alert] = "Please login or signup before continuing"
    end  

    if !current_user.blank? || !params[:zipcode].blank?
      @from_signup = true  if user_signed_in? && params[:from_signup]
      render :index
    else
      render :what_is_riszil
    end

  end

  def show
    @user = User.find(params[:id])
  end

  def new_payment_detail
    render :partial => 'devise/registrations/payment_detail' ,:locals => {:payment_detail => PaymentDetail.new} ,  :layout => false and return 
  end

  def affiliate_programme
  end	


  def contact_us
  end 

  def send_visitor_info

    if verify_aritcaptcha(params)
        contact_us = {}
        contact_us[:email] =  params[:email]
        contact_us[:subject] =  params[:subject]
        contact_us[:message] =  params[:message]
        UserMailer.send_visitor_info(contact_us).deliver
        flash[:notice] = "You have submitted the message successfully."
        redirect_to root_path
    else
        flash[:alert] = "Your answer for equation was incorrect"
        redirect_to :back
    end    
  end 

  def subregion_options
      @target = params[:target]
      render :partial => 'shared/subregion_select' , :layout => false
  end

end
