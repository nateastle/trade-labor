class Admin::SkillsController < Admin::AdminsController
  before_filter :find_skill , :only => [:edit, :update, :destroy, :force_destroy] 
  before_filter :set_page_params , :only => [:index, :search] 
  #caches_action :index, :expires_in => 12.hours.to_i

  def index
    @skills = Skill.includes(:users).order('name ASC').paginate(:page => @page , :per_page => @per_page)
  end

  def new
    @skill = Skill.new
  end

  def create
    @skill = Skill.new(params[:skill])
    if @skill.save
      expire_action :action => 'index'
      redirect_to admin_skills_path, notice: 'Skill has been created successfully'
    else
      render :new
    end
  end

  def search
    @skills = Skill.search do
      fulltext params[:query] || ''
    end.results.paginate(:page => @page , :per_page => @per_page)
  end

  def update
    if @skill.update_attributes(params[:skill])
      redirect_to admin_skills_path(page: (params[:page] || 1)), notice: 'Skill has been updated successfully'
    else
      render :new
    end
  end

  def force_destroy
    ActiveRecord::Base.transaction do
      User.where(id: @skill.users.map(&:id)).destroy_all
      @skill.destroy
    end  

    if @skill.destroyed?
      @res= {success: true, message: "Skill and associated users has been deleted successfully"}
    else
      @res = {success: false, message: "Skill could not be destroyed"}
    end      
  end

  def destroy
    if @skill.users.count == 0
      @skill.destroy
      @res= {success: true, message: "Skill has been destroyed successfully"}
    else
      @res = {success: false, message: "Skill can be destroyed only if users count is zero"}
    end  
  end

  def merge
    @skill = Skill.where(name: params[:with_skill_name]).first
    @skill_ids_to_merge = (params[:to_skill_ids] || "").split(",").map(&:to_i)

    if @skill.blank? || @skill_ids_to_merge.blank? 
      redirect_to new_merge_admin_skills_path({to_skill_ids: params[:to_skill_ids], with_skill_name: params[:with_skill_name]}), alert: 'Skill not found'
    else  
      if @skill.apply_merge(@skill_ids_to_merge)
        redirect_to admin_skills_path, notice: 'Skills have been merged successfully'
      else
        redirect_to new_merge_admin_skills_path({to_skill_ids: params[:to_skill_ids], with_skill_name: params[:with_skill_name]}), alert: 'Skills could not merged'
      end
    end  
  end

  private

  def find_skill
    @skill = Skill.where(id: params[:id]).first
  end

  def set_page_params
    @page = params[:page].try(:to_i) || 1
    @per_page = 50    
  end

end
