class Admin::UsersController < Admin::AdminsController
  
  require 'will_paginate/array'

  before_filter :find_user , :only => [:destroy, :deactivate, :activate, :show, :edit, :update, :pay_affiliation_reward]
  before_filter :check_eligibilty_for_affiliation_reward , :only => [:pay_affiliation_reward]
  before_filter :check_hubuco_script_status, only: [:index, :dashboard]

  caches_action :dashboard, :expires_in => 12.hours.to_i

  def recent
    @days = (params[:days] || '').to_i
    @days = 7 if @days == 0
    @users =  User.not_admin.created_in_last(@days).order('created_at ASC').paginate(:page => params[:page] || 1 , :per_page => 10) || []
  end

  def claimed
    @claimed_users = User.not_admin.claimed
  end  

  def dashboard
    users = User.not_admin
    @failed_user_imports_count = FailedUserImport.count

    @all_users_count = users.count
    @claimed_pages_count = users.claimed.count
    @basic_users_count =  users.basic.count
    @business_users_count = users.business.count  
    @premium_users_count = users.premium.count

    @created_in_last_24_hours = users.created_in_last(1).count
    @created_in_last_7_days = users.created_in_last(7).count
    @created_in_last_30_days = users.created_in_last(30).count

    render layout: 'new_admin'
  end

  def index
    @users = User.not_admin.includes(:role).order('created_at ASC').paginate(:page => params[:page] || 1 , :per_page => 10) || []
  end

  def show 
    @downline_members = @user.downline_members.paginate(:page => params[:page] || 1 , :per_page => 10) || []
  end

  def update
    if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
      params[:user].delete(:current_password)
    end

    if @user.update_attributes(params[:user])
      redirect_to admin_users_path, :notice => "User updated successfully"
    else
      render :edit
    end
  end

  def activate
    @index = params[:index].try(:to_i)
    @user.activate!
  end

  def deactivate
    @index = params[:index].try(:to_i)
    @user.deactivate!
  end

  def destroy
    @user.destroy!
    @users = User.not_admin.find(:all , :order => "created_at ASC",:include=> :role).paginate(:page => params[:page] || 1 , :per_page => 10) || []
  end

  def search
    @users = if !params[:query].blank? || !params[:role_name].blank?    
          User.search(:include => [:role])do
                fulltext params[:query]
                with :role_name,params[:role_name] if !params[:role_name].blank? 
                paginate :page => params[:page] || 1 , :per_page => 10
              end.results
         else
          User.find(:all , :include=> :role).paginate(:page => params[:page] || 1 , :per_page => 10) || []
         end  
  end

  def pay_affiliation_reward
    if @user.varify_paypal_email
      if @user.payment_using_paypal
        @user.affiliations.update_all( is_rewarded_for: true )
        @user.update_attribute(:is_request_reward, false)
        redirect_to :back , :notice => "Affiliation Reward successfully completed"
      else
         redirect_to :back , :alert => @user.errors.full_messages.join(",")
      end 
    else
      redirect_to :back , :alert => @user.errors.full_messages.join(",")
    end
  end

  def upload_csv
    user_import = UserImport.new(csv: params[:csv])
    if user_import.save
      redirect_to admin_users_path, notice: 'User import job has been started in back ground'
    else
      redirect_to import_admin_users_path, alert: 'Can not be uploaded'
    end  
  end  

  def start_hubuco
   StartHubucoScript.new.run unless hubuco_script_running?
   redirect_to admin_users_path, notice: 'Email verification job has been started successfully'
  end

  private

  def find_user
    @user = User.find(params[:id])
  end

  def check_eligibilty_for_affiliation_reward
    redirect_to root_path ,:alert => "Reward amount for this affiliate is zero" unless @user.is_reward_amount_more_than_zero? 
  end 
end
