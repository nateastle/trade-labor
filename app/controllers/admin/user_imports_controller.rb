class Admin::UserImportsController < Admin::AdminsController

  before_filter :find_user_import, except: [:failed]

  require 'will_paginate/array'

  def failed
    @failed_user_imports = FailedUserImport.paginate(:page => params[:page] || 1 , :per_page => 10) || []
    respond_to do |format|
      format.html
      format.js
    end    
  end

  def retry
    @user_import.retry
  end    

  def update
    @user_import.update_attributes(params[:failed_user_import])
  end

  def destroy
    @user_import.destroy
  end 

  private

  def find_user_import
    @user_import = FailedUserImport.where(id: params[:id]).first
  end

end
