class Admin::AdminsController < ApplicationController

  layout 'admin'

  before_filter  :authenticate_admin_user! , :except => [:login]
  before_filter  :check_if_admin_already_loggedin , :only => [:login]

  def login
    @user = User.new
  end 

  def reward_transaction
      @reward_transactions = RewardTransaction.find(:all , :include => :affiliate).paginate(:page => params[:page] || 1 , :per_page => 10)
  end 

  protected

  def check_hubuco_script_status
    @hubuco_script_running = hubuco_script_running?
  end

  def hubuco_script_running?
    Delayed::Job.where(queue: 'user_verify_email').count > 0
  end

  def authenticate_admin_user!
    if current_user.blank?
      redirect_to admin_login_path
    else        
      redirect_to root_path , :alert => "Restricted for admin only" unless current_user.is_admin?        
    end     
  end

  def check_if_admin_already_loggedin
    redirect_to current_user.is_admin? ? admin_root_path : root_path , :alert => "You are already signed in" if current_user      
  end 
end
