class Admin::EmailVerificationLogsController < Admin::AdminsController
  def index
    @verification_logs = EmailVerificationLog.order("created_at DESC").paginate(:page => params[:page] || 1 , :per_page => 20)
  end
end
